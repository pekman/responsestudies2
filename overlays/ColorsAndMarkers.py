import ROOT

shortGoodColors = [1001,1002,1003]
defaultGoodColors = [1001,1002,1003,1004,1000]
mediumGoodColors = [ROOT.kCyan+4,ROOT.kCyan+2,ROOT.kCyan,\
                          ROOT.kBlue,ROOT.kBlue+2,\
                          ROOT.kMagenta+2,ROOT.kMagenta,\
                          ROOT.kRed,ROOT.kRed+2,ROOT.kOrange+10,\
                          ROOT.kOrange,ROOT.kYellow]
longGoodColors = [ROOT.kCyan+4,ROOT.kCyan+3,ROOT.kCyan+2,ROOT.kCyan+1,ROOT.kCyan,\
                 ROOT.kBlue,ROOT.kBlue+1,ROOT.kBlue+2,ROOT.kBlue+3,ROOT.kBlue+4,\
                 ROOT.kMagenta+4,ROOT.kMagenta+3,ROOT.kMagenta+2,ROOT.kMagenta+1,ROOT.kMagenta,\
                 ROOT.kRed,ROOT.kRed+1,ROOT.kRed+2,ROOT.kOrange+9,ROOT.kOrange+10,\
                 ROOT.kOrange+7,ROOT.kOrange,ROOT.kYellow]



shortGoodMarkers = [20,21,22]
defaultGoodMarkers = [20,21,22,23,24]
mediumGoodMarkers = [20,21,22,23,24,25,26,27,28,29,30,31]
longGoodMarkers = [20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,
                   2,3,4,5,
                   20,21,22,23,24] #if we need more we're doomed anyway
longerGoodMarkers = [20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,
                   2,3,4,5,
                   20,21,22,23,24,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,7,8,
                   20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,] #we're doomed

Fills = []
Fills.extend(range(3001,3026))
Fills.extend(range(3144,4044,100))
Fills.extend(range(3305,3405,10))
Fills.extend(range(3351,3360))
Fills.extend(range(3409,3490,9))
Fills.extend(range(3618,3699,9))
Fills.extend(range(3001,3026))
Fills.extend(range(3144,4044,100))
Fills.extend(range(3305,3405,10))
Fills.extend(range(3351,3360))
Fills.extend(range(3409,3490,9))
Fills.extend(range(3618,3699,9))
