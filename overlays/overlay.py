import pandas as pd
import array
import ROOT
import ColorsAndMarkers
import AtlasStyle
import RatioUtils
import GraphUtils

AtlasStyle.SetAtlasStyle()

# calibrationCurveParameters = {
#     "year" : "16",
#     "etaSlice" : "-2.4_2.4",
#     "rebinFactor" : "R40",
#     "polyLog" : "five_five",
#     "prune" : "True",
#     "fitOptions" : "RESQ",
#     "fitType" : "gaus",
#     "sigmasForCoreFit" : "1.3",
#     "sigmasForTailsOfCB" : "cb0.7_1.3",
#     "numberOfEntriesMin" : "5000"
# }
#
# def BuildFileNameOutOfCalibrationCurveParameters(year, etaSlice, rebinFactor, polyLog, prune, fitOptions, fitType, sigmasForCoreFit, sigmasForTailsOfCB, numberOfEntriesMin) :
#
#     fileName = "Insitu_big_plot_dict_"
#     fileName+=  "data"+year+"_13TeV_periodX.root_"
#     fileName+= "slice_"+etaSlice
#     fileName+= "__ptMatch_"
#     fileName+= "rebin"+rebinFactor+"_rebinX1_"
#     fileName+=  polyLog
#     fileName+= "_prune"+prune+"_"
#     fileName+= fitOptions+"_"
#     fileName+= fitType+"_"
#     fileName+= sigmasForCoreFit+"_"
#     fileName+= sigmasForTailsOfCB
#     fileName+= "_bs"+numberOfEntriesMin
#     fileName+= "_nominal.pickle"
#     print(fileName)

#BuildFileNameOutOfCalibrationCurveParameters(**calibrationCurveParameters)

#### Handling eta - a bit ad-hoc but at least we can deal with the fact that it flips

etaBins = [ (-2.5,-1.8),
            (-1.8,-1.3),
            (-1.3,-1),
            (-1,-0.7),
            (-0.7,-0.2),
            (-0.2,0),
            (0,0.2),
            (0.2,0.7),
            (0.7,1),
            (1,1.3),
            (1.3,1.8),
            (1.8,2.5)
            ]

def symmetrizeEtaBin ( eta1eta2 ) :
    return (-eta1eta2[1], -eta1eta2[0] )
#print(symmetrizeEtaBin(etaBins[0]))

#### Reading the current pickle

#thedir = "projects/nominal/final_calibration/pickles/"
#theFileName = "numericalInversion_df_data16.pickle"
#theResponsePoints_df = pd.read_pickle(thedir+"/"+theFileName)
#for col in theResponsePoints_df.columns:
#    print(col)

# -> columns in this dataFrame
"""
offline_pt_avg -> x axis of original histogram (filled on the grid)
response -> HLT/offline response
offline_pt_avg_error -> error on the x axis, bin width
response_error -> HLT/offline response error from the fit
p_value -> p-value of the individual response fit [Chi2]
correction_factor -> 1/response
offline_eta_low -> low edge of eta bin
offline_eta_high -> high edge of eta bin
probe_pT -> numerically inverted pT
probe_eta -> numerically inverted eta
offline_pt_low -> numerically inverted pT, low bin edge
offline_pt_high -> numerically inverted pT, high bin edge
"""

# Plotting helpers

def plotSingleResponse(responsePoints_df, EtaBin_lowEdge = None, etaBin_highEdge = None, formattingIndex = 0) :
    #selects on eta bin
    if thisEtaBin_lowEdge != None:
        responsePoints_etaBin =  responsePoints_df[responsePoints_df["offline_eta_low"].isin([thisEtaBin_lowEdge])]
        #print(thisResponsePlot_etaBin.head())
    responseGraph = ROOT.TGraphErrors(
        len(responsePoints_etaBin["response"]),
        array.array('d', responsePoints_etaBin["offline_pt_avg"]),
        array.array('d', responsePoints_etaBin["response"]),
        array.array('d', responsePoints_etaBin["offline_pt_avg_error"]),
        array.array('d', responsePoints_etaBin["response_error"]))

    responseGraph.SetMarkerStyle(ColorsAndMarkers.longGoodMarkers[formattingIndex])
    responseGraph.SetMarkerSize(0.6)
    responseGraph.SetMarkerColor(ColorsAndMarkers.longGoodColors[formattingIndex])
    responseGraph.SetLineColor(ColorsAndMarkers.longGoodColors[formattingIndex])

    return responseGraph

def formatSingleGraph(graphToFormat, formattingIndex) :
    graphToFormat.SetMarkerStyle(ColorsAndMarkers.longGoodMarkers[formattingIndex])
    graphToFormat.SetMarkerSize(0.6)
    graphToFormat.SetMarkerColor(ColorsAndMarkers.longGoodColors[formattingIndex])
    graphToFormat.SetLineColor(ColorsAndMarkers.longGoodColors[formattingIndex])

def formatNominalGraph(graphToFormat, formattingIndex) :
    graphToFormat.SetMarkerStyle(ColorsAndMarkers.longGoodMarkers[formattingIndex])
    graphToFormat.SetMarkerSize(0.0)
    graphToFormat.SetMarkerColor(ColorsAndMarkers.longGoodColors[formattingIndex])
    graphToFormat.SetLineColor(ColorsAndMarkers.longGoodColors[formattingIndex])

# Plotting

## options for plotting - nominal must go first
plottingOptions = ["nominal", "prescaled"]

plottingOptionDictionaries = {

    "nominal" : {
        "individualTitle" : "Nominal",
        "overallTitle" : "Effect of trigger prescales",
        "responseGraph" : None,
    },

    "prescaled" : {
        "individualTitle" : "No prescale weight applied",
        "overallTitle" : "Effect of trigger prescales",
        "responseGraph" : None,
    }

}

## loop on eta bins (for now, one multigraph per eta bin)
#thisEtaBin = etaBins[6]

for thisEtaBin in etaBins:

    thisEtaBin_lowEdge = thisEtaBin[0]
    thisEtaBin_highEdge = thisEtaBin[1]
    allResponseGraphs = ROOT.TMultiGraph()
    allResponseGraphRatios = ROOT.TMultiGraph()
    thisLegend=ROOT.TLegend(0.6,0.17,0.93,0.35)
    thisLegend.SetTextFont(42)

    ## loop on plotting options (matched to directory names)

    for plottingOptionIndex, plottingOption in enumerate(plottingOptions) :

        ## Reading the current pickle
        overallTitle = plottingOptionDictionaries[plottingOption]["overallTitle"]
        plottingOptionTitle = plottingOptionDictionaries[plottingOption]["individualTitle"]
        thedir = "projects/"+plottingOption+"/final_calibration/pickles/"

        ### only one year
        year = "16"
        theFileName = "numericalInversion_df_data"+year+".pickle"

        theResponsePoints_df = pd.read_pickle(thedir+"/"+theFileName)

        thisResponseGraph = plotSingleResponse(theResponsePoints_df, thisEtaBin_lowEdge, thisEtaBin_highEdge, plottingOptionIndex)
        allResponseGraphs.Add(thisResponseGraph)
        thisLegend.AddEntry(thisResponseGraph, plottingOptionTitle)
        thisLegend.SetHeader(overallTitle)
        plottingOptionDictionaries[plottingOption]["responseGraph"] = thisResponseGraph

        thisResponseGraphRatio = RatioUtils.divideGraphs(thisResponseGraph,plottingOptionDictionaries["nominal"]["responseGraph"],True)

        if plottingOption == "nominal" :
            formatNominalGraph(thisResponseGraphRatio,plottingOptionIndex)
        else :
            formatSingleGraph(thisResponseGraphRatio,plottingOptionIndex)

        allResponseGraphRatios.Add(thisResponseGraphRatio)

    #

    ## Canvas plotting and formatting

    c=ROOT.TCanvas()

    pad1 = ROOT.TPad("pad1", "pad1", 0., 0.3, 1., 1.);
    pad2 = ROOT.TPad("pad2","pad2", 0., 0., 1., .3);

    pad1.SetBottomMargin(0.);
    pad2.SetTopMargin(0.);
    pad2.SetBottomMargin(0.5);

    pad1.Draw();
    pad2.Draw();

    pad1.cd();

    ### Main pad

    allResponseGraphs.Draw("AP")

    pad1.SetLogx()
    allResponseGraphs.GetXaxis().SetTitle("p_{T avg}^{offline} [GeV]")
    allResponseGraphs.GetYaxis().SetTitle("HLT/offline p_{T} response")
    allResponseGraphs.GetXaxis().SetLimits(60, 3000)
    allResponseGraphs.SetMinimum(0.962)
    allResponseGraphs.SetMaximum(1.006)
    allResponseGraphs.GetXaxis().SetMoreLogLabels()

    c.Draw()
    thisLegend.Draw("same")
    #this looks like a** but it's a good first attempt
    #c.BuildLegend()

    AtlasStyle.ATLASLabel(0.65, 0.88)
    AtlasStyle.myText(0.65, 0.77, ROOT.kBlack, str(thisEtaBin_lowEdge)+"< #eta^{offline} #leq "+str(thisEtaBin_highEdge))

    ### Ratio pad
    pad2.cd();
    pad2.SetLogx()

    #allRatioGraph = allResponseGraphs.Clone("")
    #graphForAxis=allResponseGraphs.Clone()
    #graphForAxis.Reset()

    #### Mess with axis labels
    graphForAxis = allResponseGraphRatios.Clone()
    graphForAxis.Draw("A")
    graphForAxis.GetXaxis().SetLimits(60, 3000)
    graphForAxis.SetMinimum(0.962)
    graphForAxis.SetMaximum(1.006)
    graphForAxis.GetXaxis().SetMoreLogLabels()

    graphForAxis.GetYaxis().SetLabelSize(0)
    y_axis = graphForAxis.GetYaxis()
    y_axis.ChangeLabel(1, -1, -1, -1, -1, -1, " ")
    y_axis.SetLabelFont(43)
    y_axis.SetLabelSize(17)
    y_axis.SetNdivisions(5)
    y_axis.SetTitle("ratio")
    y_axis.SetTitleFont(43)
    y_axis.SetTitleSize(17)
    y_axis.SetRangeUser(0.99, 1.009)
    y_axis.SetTitleOffset(2.8)
    y_axis.Draw()

    x_axis = graphForAxis.GetXaxis()
    x_axis.SetTitleSize(20)
    x_axis.SetTitleFont(43)
    x_axis.SetTitleOffset(1.4)
    x_axis.SetLabelFont(43)
    x_axis.SetLabelSize(20)
    x_axis.Draw()

    allResponseGraphRatios.Draw("P")

    ## save
    c.SaveAs("allCalibrationGraphs_eta"+str(thisEtaBin_lowEdge)+"to"+str(thisEtaBin_highEdge)+".png")
