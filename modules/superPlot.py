import ROOT
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from scipy.optimize import curve_fit
import sys

import mplhep as hep

import modules.plotting as plotting

def five_five(x,A,B,C,D,F,G,H,J,K,L,pt0):
    # This enables a smooth flat value after the cutoff
    E=-(5*A*np.log(pt0)**4) - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5) + (5*L)/(np.log(pt0)**6)
    return A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4) + L*np.log(x)**(-5)

def xz_given_slice_df(df,currentSlice,slicing_axis,projection_axis):
    # Returns the x (projection bin) and z (correction factor) for a given slice in the slicing_axis, for a dataframe
    x = df[projection_axis].loc[(currentSlice[0]<=df[slicing_axis]) & (df[slicing_axis]<=currentSlice[1])]
    z = df["z"].loc[(currentSlice[0]<=df[slicing_axis]) & (df[slicing_axis]<=currentSlice[1])]
    data_x_error = df[projection_axis+"_error"].loc[(currentSlice[0]<=df[slicing_axis]) & (df[slicing_axis]<=currentSlice[1])]
    data_z_error = df["z_error"].loc[(currentSlice[0]<=df[slicing_axis]) & (df[slicing_axis]<=currentSlice[1])]
    return x,z,data_x_error, data_z_error

def new_xz_hist(h2D,df,currentSlice,slicing_axis,projection_axis,mode):
    # Returns the x (projection bin) and z (correction factor) for a given slice in the slicing_axis, for a 2D histogram

    new_df = pd.DataFrame()
    new_df["x"] = df[projection_axis].loc[(currentSlice[0]<=df[slicing_axis]) & (df[slicing_axis]<=currentSlice[1])]
    new_df["y"] = df[slicing_axis].loc[(currentSlice[0]<=df[slicing_axis]) & (df[slicing_axis]<=currentSlice[1])]
    new_df["z"] = df["z"].loc[(currentSlice[0]<=df[slicing_axis]) & (df[slicing_axis]<=currentSlice[1])]

    if mode=="eta":
        projection_axis = h2D.GetXaxis()
        slice_axis = h2D.GetYaxis()
        number_of_bins = h2D.GetNbinsX()
    elif mode=="pt":
        projection_axis = h2D.GetYaxis()
        slice_axis = h2D.GetXaxis()
        number_of_bins = h2D.GetNbinsY()

    x=[]
    y=[]
    for index, row in new_df.iterrows():
        #corr = h2D.Interpolate(row["x"],row["y"])
        if mode =="eta": corr = h2D.Interpolate(row["x"],row["y"])
        elif mode =="pt": corr = h2D.Interpolate(row["y"],row["x"])
        x.append(row["x"])
        y.append(corr)
        
#    center_of_slice = currentSlice[0]+(currentSlice[1]-currentSlice[0])/2

#    if mode=="eta":
#        projection_axis = h2D.GetXaxis()
#        slice_axis = h2D.GetYaxis()
#        number_of_bins = h2D.GetNbinsX()
#    elif mode=="pt":
#        projection_axis = h2D.GetYaxis()
#        slice_axis = h2D.GetXaxis()
#        number_of_bins = h2D.GetNbinsY()

#    x=[]
#    y=[]
#    for projection_bin in range(1,number_of_bins+1):
#        bin_center_projection = projection_axis.GetBinCenter(projection_bin)
#        bin_center_slice = center_of_slice
#        if mode =="eta": corr = h2D.Interpolate(bin_center_projection,bin_center_slice)
#        elif mode =="pt": corr = h2D.Interpolate(bin_center_slice,bin_center_projection)
#        x.append(bin_center_projection)
#        y.append(corr)

    return np.array(x),np.array(y)

def old_xz_hist(h2D,currentSlice,mode):
    # Returns the x (projection bin) and z (correction factor) for a given slice in the slicing_axis, for a 2D histogram

    center_of_slice = currentSlice[0]+(currentSlice[1]-currentSlice[0])/2

    if mode=="eta":
        projection_axis = h2D.GetXaxis()
        slice_axis = h2D.GetYaxis()
        number_of_bins = h2D.GetNbinsX()
    elif mode=="pt":
        projection_axis = h2D.GetYaxis()
        slice_axis = h2D.GetXaxis()
        number_of_bins = h2D.GetNbinsY()

    x=[]
    y=[]
    for projection_bin in range(1,number_of_bins+1):
        bin_center_projection = projection_axis.GetBinCenter(projection_bin)
        bin_center_slice = center_of_slice
        if mode =="eta": corr = h2D.Interpolate(bin_center_projection,bin_center_slice)
        elif mode =="pt": corr = h2D.Interpolate(bin_center_slice,bin_center_projection)
        x.append(bin_center_projection)
        y.append(corr)

    return np.array(x),np.array(y)

def h2d_to_df(year,project_path,h2D):
    dictionary = {"pt_low":[],"pt":[],"pt_high":[],"eta_low":[],"eta":[],"eta_high":[],"correction_factor":[]}
    for y in range(1,h2D.GetNbinsY()+1):
        for x in range(1,h2D.GetNbinsX()+1):
            pt_low = h2D.GetXaxis().GetBinLowEdge(x)
            pt = h2D.GetXaxis().GetBinCenter(x)
            pt_high = h2D.GetXaxis().GetBinUpEdge(x)
            eta_low = h2D.GetYaxis().GetBinLowEdge(y)
            eta = h2D.GetYaxis().GetBinCenter(y)
            eta_high = h2D.GetYaxis().GetBinUpEdge(y)
            correction_factor = h2D.GetBinContent(x,y)
            dictionary["pt_low"].append(pt_low)
            dictionary["pt"].append(pt)
            dictionary["pt_high"].append(pt_high)
            dictionary["eta_low"].append(eta_low)
            dictionary["eta"].append(eta)
            dictionary["eta_high"].append(eta_high)
            dictionary["correction_factor"].append(correction_factor)

    df=pd.DataFrame(dictionary)
    dfPath = f'{project_path}final_calibration/pickles/gaussianKernel_df_{year}'
    df.to_pickle(dfPath+".pickle")

def reduced_chi_squared(o,c,sigma):
    #https://en.wikipedia.org/wiki/Reduced_chi-squared_statistic
    o = np.array(o)
    c = np.array(c)
    sigma = np.array(sigma)
    return sum( ((o-c)**2)/sigma**2 )

def getNdof(obs):
    return len(obs)-2

def plot(year,config,project_path,rootFilePath,list_of_slices,period,slicing_axis,projection_axis,data_df,calib_h2d,mode):

    # Create a different pdf page for each slice
    with PdfPages(f'{project_path}final_calibration/plots/{year}_{mode}_slice_dataVsFit.pdf') as pdf:
        figure_width = 28/2.54
        figure_ratio = 800/600
        figure_height = figure_width/figure_ratio
        f, axisList = plt.subplots(2,1,figsize=(figure_width, figure_height),sharex=True)
        ax1=axisList[0]
        ax2=axisList[1]
        box = ax1.get_position()

        # Chi^2 stuff
        obs = []
        sigma = []
        exp = []

        # loop over the slices, eta or pt
        for i, currentSlice in enumerate(list_of_slices):
            # Plot the numerically inverted data points
            # The "data" is from the sliced and projected 3D hisograms. It is what was traditionally response plots. But is now correction factor and numerically inverted
            data_x, data_z, data_x_error, data_z_error = xz_given_slice_df(data_df,currentSlice,slicing_axis,projection_axis)
            # FIXME: Hardcoded error inflation
            #data_z_error=data_z_error*np.sqrt(10.488199395260624)
            obs.extend(data_z)
            sigma.extend(data_z_error)
            ax1.errorbar(data_x, data_z, xerr=data_x_error, yerr=data_z_error,linestyle='None',marker="o",color="black",markersize=0.5,linewidth=0.5,label="Data Points")

            # Plot the 2D Gaussian kernel
            #calib_x, calib_z = old_xz_hist(calib_h2d,currentSlice,mode)
            calib_x, calib_z = new_xz_hist(calib_h2d,data_df,currentSlice,slicing_axis,projection_axis,mode)
            exp.extend(calib_z)
            ax1.plot(calib_x, calib_z,marker="o",color="red",markersize=2.0,linewidth=0.5,label="G.Kernel Fit")


            # If we are slicing in eta we can fit witha  poly-log
            if mode == "eta":
#                # Fit polylog
#                cutoff = list(data_x)[-1]
#                current_fit=five_five
#                linspace=np.linspace(calib_x[0],cutoff,num=10000)      
#                myBounds=   ((-np.inf, -np.inf, -np.inf, -np.inf, -np.inf, -np.inf, -np.inf, -np.inf, -np.inf, -np.inf,cutoff-1),
#                             ( np.inf,  np.inf,  np.inf,  np.inf,  np.inf,  np.inf,  np.inf,  np.inf,  np.inf,  np.inf,cutoff+1))
#                popt, pcov = curve_fit(current_fit, data_x, data_z,maxfev=1000000, bounds=myBounds)
#                # Plot polylog
#                ax1.plot(linspace, current_fit(linspace,*popt), linestyle='-',marker="o",color="blue",markersize=0.0,linewidth=0.5,label="Poly-log")
#                # Plot the possible flat ending of the poly log
#                linspace2 = np.linspace(cutoff,5000,num=10000)
#                last_value = [current_fit(cutoff,*popt)]
#                ax1.plot(linspace2, last_value*len(linspace2), lw=0.5, label="Flat",color="green")

                # We can only use log scale when slicing in eta
                ax1.set_xscale("log")

            # Axis limits
            ax1.set_ylim((0.95,1.05))
            ax2.set_ylim((-1,1))
            #ax1.set_xlim((50,2500))
            #ax2.set_xlim((50,2500))

            leg1 = ax1.legend(borderpad=0.5, loc=2, ncol=1, frameon=True,facecolor="white",framealpha=0.5,fontsize='medium')

#            # Shrink current axis's height by 10% on the bottom
#            ax1.set_position([box.x0, box.y0 + box.height * 0.08,
#                             box.width, box.height * 0.9])
#            # Put a legend below current axis
#            leg0=ax1.legend(loc='upper center', bbox_to_anchor=(0.5, 1.4),
#                      fancybox=True, shadow=True, ncol=5)
            leg1.set_title(f"{year}\nProbe {mode}={currentSlice}")

            # Axis titles
            if mode == "eta":
                ax2.set_xlabel(f"HLT Jet pT", fontsize=14, ha='right', x=1.0)
            elif mode == "pt":
                ax2.set_xlabel(f"HLT Jet Eta", fontsize=14, ha='right', x=1.0)
            ax1.set_ylabel("pT Correction Factor", fontsize=14, ha='right',y=1.0)

            #plt.subplots_adjust(left=0.5, right=0.5)

            ax1.grid(which="both")
            ax2.grid(which="both")

            positive=[]
            negative=[]
            relative_difference=[]
            relative_difference = list(((data_z-calib_z)/calib_z)*100)
            relative_difference_error = list((data_z_error/calib_z)*100)
            big_plot_dict={"x":list(data_x),"x_error":list(data_x_error)}
            plotting.plot_residuals(ax2,big_plot_dict,relative_difference,relative_difference_error,config)    
            ax2.set_ylabel("Residual [%]", fontsize=14, ha='right',y=1.0)      

            # Add ATLAS label
            label=hep.atlas.text("Internal",ax=ax1,loc=0,pad=0.05)

#            for i in range(0,len(relative_difference)):
#                if relative_difference[i]<0:
#                    negative.append(relative_difference[i])
#                    positive.append(0)
#                else:
#                    positive.append(relative_difference[i])
#                    negative.append(0)
#            ax2.bar(data_x, positive, width=np.array(data_x_error)*2, color="green",label="Positive residual")
#            ax2.bar(data_x, negative, width=np.array(data_x_error)*2, color="red",label="Negative residual")
            leg2 = ax2.legend(borderpad=0.5, loc=1, ncol=1, frameon=True,facecolor="white",framealpha=0.5,fontsize='medium')
            

            # This is important when using the same figure template for multiple pdf pages
            plt.tight_layout()
            pdf.savefig()
            ax1.clear()
            ax2.clear()

    chi2 = reduced_chi_squared(obs,exp,sigma)
    ndof = getNdof(obs)
    chi2ndof = chi2/ndof

    with open(f'{project_path}final_calibration/plots/{year}_chi2.txt', 'w') as f:
        f.writelines(f"chi2 = {chi2}\n")
        f.writelines(f"ndof = {ndof}\n")
        f.writelines(f"chi2 / ndof = {chi2/ndof}\n")


def loop(mode,config,year,period):

    # Set eta and pt specifics
    if mode == "eta":
        slicing_axis = "y"
        projection_axis = "x"
        list_of_slices = config.list_of_slices
    if mode == "pt":
        slicing_axis = "x"
        projection_axis = "y"
        list_of_slices = [[70,75],[75,80],[80,90],[90,100],[100,110],[110,120],[120,130],[130,145],[145,155],[155,170],[170,190],[190,205],[205,230],[230,250],[250,275],[275,300],[300,330],[330,360],[360,395],[395,435],[435,475],[475,520],[520,570],[570,625],[625,690],[690,755],[755,825],[825,905],[905,995],[995,1090],[1090,1195],[1195,1310],[1310,1440],[1440,1575],[1575,1730],[1730,1895],[1895,2080],[2080,2280]]

    # Open the calibration histogram, which also stores the numerically inverted 2D graph, and 2D gaussian kernel fit
    fpath = f'{config.project_path}merged/{year}/period{period}/calibration_output/calibration_material/{year}_Calibration_TH2D.root'
    data_inFile = ROOT.TFile.Open( fpath)
    data_inFile.cd()
    listOfKeys = data_inFile.GetListOfKeys()

    # get the numerically inverted 2D graph ("data") as a dataframe
    data_graph = data_inFile.Get("Graph2D")
    data_df = pd.DataFrame({"x":list(data_graph.GetX()),"y":list(data_graph.GetY()),"z":list(data_graph.GetZ()),"x_error":list(data_graph.GetEX()),"y_error":list(data_graph.GetEY()),"z_error":list(data_graph.GetEZ())})
    data_inFile.Close()

    
    # Get the 2D histogram actually used for calibration
    fpath = f'{config.project_path}merged/{year}/period{period}/calibration_output/calibration_material/{year}_Calibration_TH2D_frozen.root'
    calib_inFile = ROOT.TFile.Open( fpath)
    calib_inFile.cd()
    calib_h2d = calib_inFile.Get("calibration")
    
    # Call plotting script
    rootFilePath=config.rootFilePath(year=year,period=period)
    plot(year,config,config.project_path,rootFilePath,list_of_slices,period,slicing_axis,projection_axis,data_df,calib_h2d,mode)

    h2d_to_df(year,config.project_path,calib_h2d)

    calib_inFile.Close()

def main(config,year,period):
    # Plot both eta and pt sliced plots
    modes = ["eta","pt"]
    for mode in modes:
        loop(mode,config,year,period)
