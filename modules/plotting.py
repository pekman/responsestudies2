from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import mplhep as hep
import pandas as pd
import numpy as np
from scipy.optimize import curve_fit
from scipy import integrate
import pickle
import ROOT
import scipy
import sys

def reduced_chi_squared(o,c,sigma):
    #https://en.wikipedia.org/wiki/Reduced_chi-squared_statistic
    o = np.array(o)
    c = np.array(c)
    sigma = np.array(sigma)
    return sum( ((o-c)**2)/sigma**2 )

def getNdof(obs):
    return len(obs)

def get_chisquared_over_ndf(observed,expected):
    lst=[]
    for i in range(0,len(observed)):
        lst.append( (observed[i]-expected[i])**2 / expected[i])

    # Calculate chi^2 as sum of list
    chisquared = sum(lst)

    # Calculate number of degrees of freedom
    ndf = (len(lst)-1)
    return chisquared/ndf

def plot_residuals(ax,big_plot_dict,relative_difference,relative_difference_error,config):
    positive=[]
    negative=[]
    positive_err=[]
    negative_err=[]
    for i in range(0,len(relative_difference)):
        if relative_difference[i]<0:
            negative.append(relative_difference[i])
            negative_err.append(relative_difference_error[i])
            positive.append(0)
            positive_err.append(0)
        else:
            positive.append(relative_difference[i])
            positive_err.append(relative_difference_error[i])
            negative.append(0)
            negative_err.append(0)
    if config.residualPanelErrors:
        ax.errorbar(big_plot_dict["x"], positive, yerr=positive_err,
                    linestyle='None', marker="o", color="green", markersize=0.0, linewidth=0.5,
                    label="Positive residual"
                    )
        ax.errorbar(big_plot_dict["x"], negative, yerr=negative_err,
                    linestyle='None', marker="o", color="red", markersize=0.0, linewidth=0.5, 
                    label="Negative residual"
                    )
        ax.bar(big_plot_dict["x"], positive, width=np.array(big_plot_dict["x_error"])*2, color="green",label="Positive residual",alpha=0.5)
        ax.bar(big_plot_dict["x"], negative, width=np.array(big_plot_dict["x_error"])*2, color="red",label="Negative residual",alpha=0.5) 
    else:
        ax.bar(big_plot_dict["x"], positive, width=np.array(big_plot_dict["x_error"])*2, color="green",label="Positive residual")
        ax.bar(big_plot_dict["x"], negative, width=np.array(big_plot_dict["x_error"])*2, color="red",label="Negative residual")

def save_datapoints_to_dataframe(config,year,period,currentSlice,big_plot_dict):
    df = pd.DataFrame(big_plot_dict)
    df["correction_factor"]=1/df["y"]
    df=df.rename(columns={"x": "offline_pt_avg", "y": "response","x_error":"offline_pt_avg_error","y_error":"response_error"})
    df["offline_eta_low"]=currentSlice[0]
    df["offline_eta_high"]=currentSlice[1]
    dfPath = f'{config.project_path}merged/{year}/period{period}/stitched_response_dataframes/{year}_slice[{str(currentSlice[0])},{str(currentSlice[1])}]_{config.xAxisVariable}'
    df.to_pickle(dfPath+".pickle")

def make_response_residual_histograms(config,year,period,currentSlice,big_plot_dict,calibModel,calibParameters):
    # make residual and pull histograms
    hResiduals = ROOT.TH1F('calibrationResiduals','Calibration Residual',20,-0.001,0.001)
    hPulls = ROOT.TH1F('calibrationPulls','Calibration Pull',10,-5,5)
    for (x,y,y_error) in zip (big_plot_dict["x"],big_plot_dict["y"],big_plot_dict["y_error"]):
        resid = y - calibModel(x,*calibParameters)
        resid_error = y_error
        hResiduals.Fill(resid)
        hPulls.Fill(resid/resid_error)
    figure_path = f'{config.project_path}merged/{year}/period{period}/response_plots/{year}_slice[{str(currentSlice[0])},{str(currentSlice[1])}]_{config.xAxisVariable}'
    cResiduals = ROOT.TCanvas()
    hResiduals.Draw('e')
    cResiduals.Print(figure_path+'_residuals.pdf')
    tmpFit = ROOT.gStyle.GetOptFit()
    ROOT.gStyle.SetOptFit(111111)
    cPulls = ROOT.TCanvas()
    hPulls.Draw('e')
    hPulls.Fit('gaus')
    cPulls.Print(figure_path+'_pulls.pdf')
    ROOT.gStyle.SetOptFit(tmpFit)
    root_path = f'{config.project_path}merged/{year}/period{period}/response_plots/{year}_{config.xAxisVariable}_residuals.root'
    hist_suffix = f'slice{str(currentSlice[0])}_{str(currentSlice[1])}'
    rfile = ROOT.TFile.Open(root_path,'UPDATE')
    rfile.WriteObject(hResiduals,'residuals_'+hist_suffix)
    rfile.WriteObject(hPulls,'pulls_'+hist_suffix)
    rfile.Close()
    

def plot(config, year, period, currentSlice, current_fit, pdf):

    # Each trigger is cut at its boundary and plotted separately.
    # Therefore we define a dictionary to hold all triggers combined
    # so that we can fit the response as a whole later
    big_plot_dict = {
                    "x":[],
                    "y":[],
                    "x_error":[],
                    "y_error":[],
                    "p_value":[],
                    "diff":[],
                    "chi":[],
                    "sigma":[],
                    "sigma_error":[],
                    }
    
    # Define a figure and axes
    #f, axisList = plt.subplots(2,1,figsize=(2*25*(1/2.54), 2*13.875*(1/2.54)),sharex=True)
    f, axisList = plt.subplots(1,1,figsize=(8*2.5*(1/2.54), 6*2.5*(1/2.54)),sharex=True)
    axisList=[axisList]
    #f, axisList = plt.subplots(2,1,figsize=(21*(1/2.54)/3, 14.8*(1/2.54)),sharex=True)
    #f, ax = plt.subplots(figsize=(8*2.5*(1/2.54), 6*2.5*(1/2.54)),sharex=True)
    #axisList=[ax]
    # Define a path and name for the dataframe based on the slice range
    dfPath = f'{config.project_path}merged/{year}/period{period}/sliceNdice_pickles/{year}_slice[{str(currentSlice[0])},{str(currentSlice[1])}]_{config.xAxisVariable}'
    df = pd.read_pickle(dfPath+".pickle") # Read the appropriate dataframe. One per slice, per file path
    boundary_path= f'{config.project_path}merged/{year}/period{period}/boundary_dictionaries/{year}_slice[{str(currentSlice[0])},{str(currentSlice[1])}]_{config.xAxisVariable}'
    boundaryDict = pd.read_pickle(boundary_path+".pickle")
    
    # Create a bunch of new columns in the dataframe
    # FIXME: should not be needed anymore since we added triggers to the dataframe
    # and energyscale is in the name
    df["name"]=df.index
    df["trigger"]= df.apply(lambda row : int(row["name"].split("_")[2][1:]),axis=1)
    df["energyScale"]= df.apply(lambda row : row["name"].split("_")[4].split("-")[0],axis=1)
    df["xVariable"]= df.apply(lambda row : row["name"].split("_")[8],axis=1)
    df["zVariable"]= df.apply(lambda row : row["name"].split("_")[10],axis=1)
    df["year"]= df.apply(lambda row : year,axis=1)

    # Make cuts based on the new columns
    df=df[df.xVariable == f'{config.xAxisVariable}']
    df=df[df.zVariable == config.slicingAxis]
    df=df[df.energyScale == f'{config.energy_scale}']

    #sys.exit()

    # Iterate over 3D histograms in data frame
    for TH3Name in df.index:
        
        # Find which trigger we are doing
        currentTrigger=TH3Name.split("_-_")[0].split("_")[2]

        x=df["x"].loc[TH3Name]
        y=df["y"].loc[TH3Name]
        ymean=df["yMean"].loc[TH3Name]
        x_error=df["xError"].loc[TH3Name]
        y_error=df["yError"].loc[TH3Name]
        p_values=df["p_value"].loc[TH3Name]
        ymean_error=df["yMeanError"].loc[TH3Name]
        fitParameters=df["fitParameters"].loc[TH3Name]
        
        diff = list(abs(np.array(df["y"].loc[TH3Name]) - np.array(df["yMean"].loc[TH3Name])))
        chi = df["Chi2Ndof"].loc[TH3Name]
        sigma = df["sigma"].loc[TH3Name]
        sigma_error = df["sigmaError"].loc[TH3Name]
        

        # remove response fits with bad fit quality.  when 'good fit quality'
        # criteria include a variable from one of the lists, we need to make a
        # temporary copy (the a_* lists) so as to preserve the original list
        # until we are done filtering all lists.
        # note: this uses the inflated yerror, as that is the only one available here.
        if config.pruneBadFits:
            indexChi2prob=6 # the chi^2 probability index in the fitParameters
            indexFitStatus=7 # this fit status in the list
            x = [v for (i,v) in enumerate(x) if (not config.pruneBadFitStatus or fitParameters[i][indexFitStatus]==True) and fitParameters[i][indexChi2prob]>config.pruneMinProb and y_error[i]<config.pruneMaxError]
            x_error = [v for (i,v) in enumerate(x_error) if (not config.pruneBadFitStatus or fitParameters[i][indexFitStatus]==True) and fitParameters[i][indexChi2prob]>config.pruneMinProb and y_error[i]<config.pruneMaxError]
            y = [v for (i,v) in enumerate(y) if (not config.pruneBadFitStatus or fitParameters[i][indexFitStatus]==True) and fitParameters[i][indexChi2prob]>config.pruneMinProb and y_error[i]<config.pruneMaxError]
            a_y_error = [v for (i,v) in enumerate(y_error) if (not config.pruneBadFitStatus or fitParameters[i][indexFitStatus]==True) and fitParameters[i][indexChi2prob]>config.pruneMinProb and y_error[i]<config.pruneMaxError]
            a_fitParameters = [v for (i,v) in enumerate(fitParameters) if (not config.pruneBadFitStatus or fitParameters[i][indexFitStatus]==True) and fitParameters[i][indexChi2prob]>config.pruneMinProb and y_error[i]<config.pruneMaxError]
            ymean = [v for (i,v) in enumerate(ymean) if (not config.pruneBadFitStatus or fitParameters[i][indexFitStatus]==True) and fitParameters[i][indexChi2prob]>config.pruneMinProb and y_error[i]<config.pruneMaxError]
            ymean_error = [v for (i,v) in enumerate(ymean_error) if (not config.pruneBadFitStatus or fitParameters[i][indexFitStatus]==True) and fitParameters[i][indexChi2prob]>config.pruneMinProb and y_error[i]<config.pruneMaxError]
            p_values = [v for (i,v) in enumerate(p_values) if (not config.pruneBadFitStatus or fitParameters[i][indexFitStatus]==True) and fitParameters[i][indexChi2prob]>config.pruneMinProb and y_error[i]<config.pruneMaxError]
            y_error = a_y_error
            fitParameters = a_fitParameters
        
        
        # Add data to the larger combination dataseries
        big_plot_dict["x"].extend(x)
        big_plot_dict["y"].extend(y)
        big_plot_dict["x_error"].extend(x_error)
        big_plot_dict["y_error"].extend(y_error)
        big_plot_dict["p_value"].extend(p_values)
        
        big_plot_dict["diff"].extend(diff)
        big_plot_dict["chi"].extend(chi)
        big_plot_dict["sigma"].extend(sigma)
        big_plot_dict["sigma_error"].extend(sigma_error)

        # Plot the current tirgger with corresponding color scheme
        try:
            axisList[0].errorbar(x, y, yerr=y_error, xerr=x_error,
                    linestyle='None',
                    marker="o",
                    color=config.colorDict[currentTrigger],
                    markersize=0.5,
                    linewidth=1.5,
                    label=currentTrigger,
                   )
        except StopIteration:
            print("StopIteration")
            axisList[0].errorbar(x, y, yerr=[10000]*len(x), xerr=x_error,
                    linestyle='None',
                    marker="o",
                    color=config.colorDict[currentTrigger],
                    markersize=0.5,
                    linewidth=1.5,
                    label=currentTrigger,
                   )
        # also plot means, if requested
        if config.plotResponseMean:
            axisList[0].errorbar(x, ymean, yerr=ymean_error, xerr=x_error,
                                 linestyle='None',
                                 marker="x",
                                 color="#cfcfcf",
                                 markersize=0.0,
                                 linewidth=0.4,
                                 #label=currentTrigger,
                                 )

    # Find the last datapoint, and define it as the cutoff for the fit
    # this is also where flat fit should start
    # (there may not be any data points for this slice)
    cutoff = 2000.
    if len(big_plot_dict["x"]) > 0:
        cutoff = big_plot_dict["x"][-1]

    # If this is the OnlineToOffline energy scale or any mjj plot, we want to a flat line
    # at 1 and the residuals to 1
#    if config.energy_scale == "OnlineToOffline" or config.xAxisVariable == "mjj":
    #linspace=np.linspace(big_plot_dict["x"][0],big_plot_dict["x"][-1],num=10000)
    #linspace=np.linspace(10,10000,num=10000)
    flat = np.array([1]*len(big_plot_dict["x"]))
    axisList[0].plot(big_plot_dict["x"], flat, lw=1, label='Union', color="red")
    y_fit        = flat
    relative_difference = (big_plot_dict["y"]-y_fit)/y_fit
    relative_difference_error = big_plot_dict["y_error"]/y_fit
    #current_fit="Union_Union"
    popt=0

    # If this is not the OnlineToOffline energy scale ,we want to plot a polylog fit and plot
    # the residuals to that fit
#    else:     
#        linspace=np.linspace(60,cutoff,num=10000)             
#        myBounds=   ((-np.inf, -np.inf, -np.inf, -np.inf, -np.inf, -np.inf, -np.inf, -np.inf, -np.inf, -np.inf,cutoff-1),
#                     ( np.inf,  np.inf,  np.inf,  np.inf,  np.inf,  np.inf,  np.inf,  np.inf,  np.inf,  np.inf,cutoff+1))
#        #try:
#        popt, pcov = curve_fit(current_fit, np.array(big_plot_dict["x"]), np.array(big_plot_dict["y"]), sigma=np.array(big_plot_dict["y_error"]),maxfev=1000000, bounds=myBounds)
#        axisList[0].plot(linspace, current_fit(linspace,*popt), lw=0.5, label=str(current_fit).split(" ")[1],color="blue")
#        y_fit        = current_fit(np.array(big_plot_dict["x"]),*popt)
#        relative_difference = (big_plot_dict["y"]-y_fit)/y_fit
#        relative_difference_error = big_plot_dict["y_error"]/y_fit
#        
#        linspace2 = np.linspace(cutoff,5060,num=10000)
#        last_value = [current_fit(cutoff,*popt)]
#        axisList[0].plot(linspace2, last_value*len(linspace2), lw=0.5, label="Flat",color="red")
#    
#        # Plot residuals
#    plot_residuals(axisList[1],big_plot_dict,relative_difference,relative_difference_error,config)
    #chi2 = reduced_chi_squared(big_plot_dict["y"],flat,big_plot_dict["y_error"])
    #ndof = getNdof(big_plot_dict["y"])
    #chi2ndof = chi2/ndof
    chi2ndof = np.nan
#            
#        # FIXME: Something is clearly wrong with this chisquared implementation
#        chisquared_over_ndf = get_chisquared_over_ndf(big_plot_dict["y"],y_fit)
    
    # Add legends
    leg0 = axisList[0].legend(borderpad=0.5, loc=1, ncol=2, frameon=True,facecolor="white",framealpha=1,fontsize='medium')
    leg0._legend_box.align = "left"
    #leg1 = axisList[1].legend(borderpad=0.5, loc=1, ncol=1, frameon=True,facecolor="white",framealpha=1,fontsize='medium')
    #leg1._legend_box.align = "left"
    #leg0.set_title(year+f" period{period}"+"\n"+config.plotDict[config.xAxisVariable]["legendTitle"]+f"\n{config.energy_scale}"+f"\nEta={currentSlice}"+f"\nX^2/ndf = {round(chisquared_over_ndf,5)}")
    leg0.set_title(year+"\n"+config.plotDict[config.xAxisVariable]["legendTitle"]+f"\n{config.energy_scale}"+f"\nEta={currentSlice}")#+f"\nChi²={round(chi2ndof,2)}")
    
    # Set limits and labels
    axisList[0].set_xlim(config.plotDict[config.xAxisVariable]["xLimits"])
    axisList[0].set_ylim(config.plotDict[config.xAxisVariable]["yLimits"])

    #axisList[1].set_xlim(config.plotDict[config.xAxisVariable]["xLimits"])
    #axisList[1].set_ylim(-0.01,0.01)

    # Set log scale
    axisList[0].set_xscale(config.plotDict[config.xAxisVariable]["xScale"])
    #axisList[1].set_xscale(config.plotDict[config.xAxisVariable]["xScale"])

    # Set axis labels
    axisList[0].set_ylabel(config.plotDict[config.xAxisVariable]["yAxisLabel"], fontsize=14, ha='right', y=1.0)
    axisList[0].set_xlabel(config.plotDict[config.xAxisVariable]["xAxisLabel"], fontsize=14, ha='right',x=1.0)
    #axisList[1].set_xlabel(config.plotDict[config.xAxisVariable]["xAxisLabel"], fontsize=14, ha='right',x=1.0)
    #axisList[1].set_ylabel("(Response-Fit)/Fit", fontsize=14, ha='right', y=1.0)

    # Create tick marks based on the custom boundary dict
    myTicks=[]
    myTicks2=[]
    realTicks=[]
    for key in boundaryDict[year]:
        myTicks.append(key)
        realTicks.append(boundaryDict[year][key][1])
        myTicks2.append(boundaryDict[year][key][1])
    ax2 = axisList[0].twiny()
    ax2.set_xlim(config.plotDict[config.xAxisVariable]["xLimits"])
    ax2.set_ylim(config.plotDict[config.xAxisVariable]["yLimits"])
    ax2.set_xscale(config.plotDict[config.xAxisVariable]["xScale"])
    ax2.set_xticks(realTicks)
    ax2.set_xticklabels(myTicks)
    axisList[0].set_xticks(realTicks)
    axisList[0].set_xticklabels(myTicks2)

    # Add grid
    #axisList[1].grid(True)
    axisList[0].grid(True)

    # Add ATLAS label
    #label=hep.atlas.text("Work in Progress",ax=axisList[0],loc=0,pad=0.12)
    label=hep.atlas.text("Internal",ax=axisList[0],loc=0,pad=0.12)

    # Use tight layout
    plt.tight_layout()

    # Save plot as .pdf
    #figure_path = f'{config.project_path}merged/{year}/period{period}/response_plots/{year}_slice[{str(currentSlice[0])},{str(currentSlice[1])}]_{config.xAxisVariable}_{config.energy_scale}'
    #f.savefig(figure_path+".png")
    pdf.savefig()
    axisList.clear()
    ax2.clear()

    save_datapoints_to_dataframe(config,year,period,currentSlice,big_plot_dict)

    if config.makeResponseResidualHistograms:
        make_response_residual_histograms(config,year,period,currentSlice,big_plot_dict,current_fit,popt)
    
