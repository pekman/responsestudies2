#!/usr/bin/env python

import ROOT
import copy
#import math

class JES_CBBalanceFitter():

  ## Initialize the class, with Gaussian fits by default ##
  def __init__(self, Nsigma):
    self.param="gaus"
    self.fitDesc="Gaussian"
    self.fitOpt = "RQ0";
    self.Nsigma = Nsigma;
    self.KickBias = False;
    self.minPt = 0;
    self.fit = 0;
    self.fitResult = None;
    self.histo = 0;
    self.fitHist = 0;
    self.fitCol = ROOT.kRed;
    self.lNlines = 0;
    self.rNlines = 0;
    self.fitMin = -10.0;
    self.fitMax = 10.0;

    self.PrintText = False
    self.textSize = 0.04
    self.xLeft = 0.15;
    self.xRight = 0.7;
    self.yTop = 0.9;
    self.dy = 0.05;


    self.fitMeanMin = 0.9
    self.fitMeanMax = 1.1
    self.fitWidthMin = 0.01
    self.fitWidthMax = 0.3

    self.rebin = True
    self.debug = False
    self.smartFit = True #False
    self.useRangeFromShape = False
    self.fractionMaxBin = 0.25
    self.fractionMaxBinLeft = None
    self.fractionMaxBinRight = None
    self.recursion = 0

######## Main Fit functions ##########

  ## The callable fit function.  Use this in your analysis code ##
  def Fit(self, histo, fitMin = None, fitMax = None):
    self.histo = histo
    self.fitHist = histo.Clone();

    ## Determine and perform the optimal rebinning ##
    if( self.rebin ):
      self.OptimalRebin(self.fitHist);

    
    f = self.BasicFit( self.fitHist, fitMin, fitMax )

    return f;

  ## The basic fit ##
  def BasicFit( self, histo, fitMin = None, fitMax = None ):
    self.recursion = self.recursion + 1
      
    if( fitMin == None ):
      fitMin = self.fitMin
    if( fitMax == None ):
      fitMax = self.fitMax

    if( self.debug ):
      print("Running BasicFit with", histo.GetName(), fitMin, fitMax, self.recursion)

    ## Create and configure fit object ##
    self.fit = ROOT.TF1( "fit", self.param, fitMin, fitMax )
    self.fit.SetLineWidth(3);
    if not self.KickBias:
        self.fit.SetParameters( histo.GetMaximum(), histo.GetMean(), histo.GetRMS() );
    else:
        # import pdb; pdb.set_trace()
        # randomly vary initial mean within 1/10 of bin size to avoid bias when true mean is too close to initial mean
        initmean = ROOT.gRandom.Gaus(histo.GetMean(),0.1*histo.GetBinWidth(histo.FindFirstBinAbove(histo.GetMean())))
        self.fit.SetParameters( histo.GetMaximum(), initmean , histo.GetRMS() );
    self.fit.SetParLimits(1, self.fitMeanMin, self.fitMeanMax);
    self.fit.SetParLimits(2, self.fitWidthMin, self.fitWidthMax);
    if self.fitDesc=='Crystal Ball' and self.recursion==1:
        (par1,par2) = self.FitInitialGaussianForCB(histo,fitMin,fitMax)
        if self.debug:
            self.LogFitInfo()
        self.fit.SetParameter(1,par1);
        self.fit.SetParameter(2,par2);
        if self.debug:
            print( "After gaussian fit")
            self.LogFitInfo()
        self.SetInitialCBParameters(histo,fitMin,fitMax)
        # initial pdf value at x=mean is
        pdf_max = self.fit.Eval(par1)
        self.fit.SetParameter(0,histo.GetMaximum()/pdf_max)
        if False:
            self.SetCrystalBallParLimits()
            if self.debug:
                print( "After CB parameters initialized")
                self.LogFitInfo()
            self.FitOneSideCB(histo,1,fitMin,fitMax)
            if self.debug:
                print( "After positive-side fit")
                self.LogFitInfo()
            self.SetCrystalBallParLimits()
            par6 = self.fit.GetParameter(6)
            par7 = self.fit.GetParameter(7)
            self.fit.FixParameter(1,par1)
            self.fit.FixParameter(2,par2)
            self.SetInitialCBParameters(histo,fitMin,fitMax)
            if self.debug:
                print( "Before negative-side fit")
                self.LogFitInfo()
            self.FitOneSideCB(histo,-1,fitMin,fitMax)
            if self.debug:
                print( "After negative-side fit")
                self.LogFitInfo()
            par3 = self.fit.GetParameter(3)
            par4 = self.fit.GetParameter(4)
        self.SetCrystalBallParLimits()
        self.fit.SetParLimits(1, self.fitMeanMin, self.fitMeanMax);
        self.fit.SetParLimits(2, self.fitWidthMin, self.fitWidthMax);
        self.fit.SetParameter(1,par1)
        self.fit.SetParameter(2,par2)
        if histo.GetSkewness()>0.:
            self.fit.SetParameter(5,0.75)
        else:
            self.fit.SetParameter(5,0.25)
        
    self.fit.SetLineColor(self.fitCol);
    ## A smarter fit that starts with a likelihood fit and then performs two refits, each
    ## adjusting the fit ranges
    if( self.smartFit ):
      fitNsigma = 7
      for iteration in range(10):
        fMin = histo.GetMean() - fitNsigma * histo.GetRMS()
        fMax = histo.GetMean() + fitNsigma * histo.GetRMS()
        if self.debug:
          print("Fit range: ",fMin,fMax)
        self.fit.SetRange(fMin,fMax)
        if self.debug:
          print("Running final BasicFit with", histo.GetName(), self.fit.GetMinimumX(), self.fit.GetMaximumX(), self.recursion)
        self.FitResult = histo.Fit(self.fit,self.fitOpt)
        if self.debug:
            print('FIT ITERATION ',self.FitResult.Status(),self.FitResult.Prob())
        try:
            if self.FitResult and self.FitResult.Prob()>1e-23: #and self.FitResult.IsValid():
              break # converged
        except ReferenceError:
            import pdb; pdb.set_trace()

          
        # else try various ways to kick the fit and see what happens
        if iteration==0:
            fitNsigma = 5
        elif iteration==1:
            # swap left and right CBs as dominant one
            (par1,par2) = self.FitInitialGaussianForCB(histo,fitMin,fitMax)
            self.fit.SetParameter(1,par1);
            self.fit.SetParameter(2,par2);
            self.SetInitialCBParameters(histo,fitMin,fitMax)
            (pl3,pr3) = (self.fit.GetParameter(3),self.fit.GetParameter(6))
            (pl4,pr4) = (self.fit.GetParameter(4),self.fit.GetParameter(4))
            self.fit.SetParameter(3,pr3)
            self.fit.SetParameter(6,pl3)
            self.fit.SetParameter(4,pr4)
            self.fit.SetParameter(7,pl4)
            fitNsigma = 7
        elif iteration>=2:
            # randomize CB tail parameters
            random = ROOT.TRandom3(iteration+1234)
            (par1,par2) = self.FitInitialGaussianForCB(histo,fitMin,fitMax)
            self.fit.SetParameter(1,par1);
            self.fit.SetParameter(2,par2);
            self.SetInitialCBParameters(histo,fitMin,fitMax)
            self.fit.SetParameter(5,random.Uniform(0,1))
            self.fit.SetParameter(3,-1.*random.Uniform(0.5,10))
            self.fit.SetParameter(6,random.Uniform(0.5,10))
            self.fit.SetParameter(4,random.Uniform(0.01,20))
            self.fit.SetParameter(7,random.Uniform(0.01,20))
        
        # import pdb; pdb.set_trace()


          # self.SetSmartFitRange(fitMin, fitMax)
          # histo.Fit(self.fit,self.fitOpt)

          # self.SetSmartFitRange(fitMin, fitMax)
          # histo.Fit(self.fit,self.fitOpt)
      
    else:
      if self.useRangeFromShape:
        self.SetFitRangeFromShape(histo)

      self.FitResult = histo.Fit(self.fit,self.fitOpt);

    self.recursion = self.recursion - 1
    return self.fit;

  def SetFitRangeFromShape(self, hist):
    maxContent = hist.GetBinContent(hist.GetMaximumBin())
    
    fractionLeft = self.fractionMaxBinLeft if self.fractionMaxBinLeft else self.fractionMaxBin
    firstBinAbove = hist.FindFirstBinAbove(maxContent*fractionLeft)    
    fitLow = hist.GetXaxis().GetBinLowEdge( firstBinAbove-1 )

    fractionRight = self.fractionMaxBinRight if self.fractionMaxBinRight else self.fractionMaxBin
    lastBinAbove = hist.FindLastBinAbove(maxContent*fractionRight)
    fitHigh = hist.GetXaxis().GetBinUpEdge( lastBinAbove+1 )
    
    self.fit.SetRange(fitLow, fitHigh)
  
  ## Set a smarter fit range after an initial fit ##
  def SetSmartFitRange( self, smallestMin = None, largestMax = None ):
    mean = self.GetMean()
    sigma = self.GetSigma()
    smartMin = mean - self.Nsigma*sigma
    smartMax = mean + self.Nsigma*sigma

    # Only use smart range if it is narrower than preconfigured fit range
    if ( smallestMin and smartMin < smallestMin):
      smartMin = smallestMin
    if ( largestMax and smartMax > largestMax):
      smartMax = largestMax;

    self.fit.SetRange(smartMin,smartMax);
    if(self.debug):
      print("Try to find smart fit range from information of basic fit:")
      print("mean: {:f}, sigma: {:f}".format(mean, sigma))
      print("Setting smart fit edges to", smartMin, "->", smartMax)

    return

#//-----------------------------------------------------------------------//
#// Functions to retrieve fit variables
#//-----------------------------------------------------------------------//
  def GetFit(self):
    if (self.fit==None):
      print("Something went wrong. Can't access fit function!")
    return self.fit

  def GetHisto(self):
    if (self.fitHist==None):
      print("Something went wrong. Can't access fitted histogram!")
    return self.fitHist

  def GetFineHisto(self):
    if (self.histo==None):
      print("Something went wrong. Can't access fitted histogram!")
    return self.histo

  def GetMean(self):
    mean = self.GetFit().GetParameter(1)
    return mean

  def GetMeanError(self):
    param = self.GetFit().GetParError(1)
    return param

  def GetSigma(self):
    param = self.GetFit().GetParameter(2)
    return param

  def GetSigmaError(self):
    param = self.GetFit().GetParError(2)
    return param

  def GetPeak(self):
    return self.GetFit().GetMaximumX(0,2)

  def GetMedian(self):
    return self.GetQuantile(0.5)

  def getQuantile( self, frac ):
    if( frac <=0 or frac >= 1):
      print("Error, can't access quantile for fraction ", frac)
      exit(1)

    thisMin = self.GetMean() - 5.*self.GetSigma()
    thisMax = self.GetMean() + 5.*self.GetSigma()

    if( "Pois" in self.param and thisMin < 0):
      thisMin = 0

    Nsteps = 10000

    fit = self.GetFit()
    Atot = fit.Integral(thisMin, thisMax)
    A = 0
    dx = (thisMax-thisMin)/Nsteps

    for i in range(0, Nsteps):
      xi = thisMin + i*dx
      A += fit.Eval(xi)*dx

      if( A > Atot * frac ):
        return xi - 0.5*dx

    print("Error, GetQuantile failed")
    return 0

  def GetHistoQuantile( frac ):
    if( frac <=0 or frac >=1 ):
      print("Error, can't access quantile for fraction ", frac)
      exit(1)

    h = self.GetFineHisto()
    h2 = h.Clone()

    #include underflow and overflow bin contents in the computation of the quantiles ("GetQuantiles" function in root ignores underflow and overflow bin contents)
    Nbins = h2.GetNbinsX();
    h2.SetBinContent( 1, h2.GetBinContent(0)+h2.GetBinContent(1) );
    h2.SetBinContent( Nbins, h2.GetBinContent(Nbins)+h2.GetBinContent(Nbins+1) );
    h2.ComputeIntegral();

    integral = h2.GetSumOfWeights();
    if( self.debug ):
      print("Computing quantiles...")
      print("integral =", integral, "entries =", h2.GetEntries(), "underflow =", h2.GetBinContent(0), "overflow =", h2.GetBinContent(Nbins+1))


    nq = 1;
    # position where to compute the quantiles in [0,1]
    xq = array.array('d',[0 for i in range(0, nq)])
    # array to contain the quantiles
    yq = array.array('d',[0 for i in range(0, nq)])
    xq[0] = frac


    if( h2.GetBinContent(0)/integral > xq[0] ):
      print("Error, quantile will be biassed (underflow). Consider extending the range of the histogram towards lower values.")
      exit(1)
    if( h2.GetBinContent(Nbins+1) / integral > (1. - xq[0]) ):
      print("Error, quantile will be biassed (overflow). Consider extending the range of the histogram towards larger values.")
      exit(1)

    h2.GetQuantiles(nq, yq, xq)
    if(self.debug):
      print("Requested quantile: " , xq[0] , ";  Result: " , yq[0])

    return yq[0]

#//-----------------------------------------------------------------------//
  def GetChi2(self):
    return self.GetFit().GetChisquare();
#//-----------------------------------------------------------------------//
  def GetNdof(self):
    return self.GetFit().GetNDF();
#//-----------------------------------------------------------------------//
  def GetChi2Ndof(self):
    if( self.GetNdof() == 0):
      return float('inf')
      #return math.inf
    else:
      return self.GetChi2()/self.GetNdof()

  def GetChi2Prob( self ):
    return ROOT.TMath.Prob( self.GetChi2(), self.GetNdof() )

  #def GetCovarMatrix( self ):
  #  covarMatrix = self.GetFit().GetCovarianceMatrix()
  #  return covarMatrix


#//-----------------------------------------------------------------------//
#// Functions to draw and print fit info
#//---------------------------------------------------------------------//
  def DrawFitAndHisto(self, drawRangeMin = None, drawRangeMax = None):
    xlow = self.GetMean()-7*self.GetSigma() if not drawRangeMin else drawRangeMin
    xup = self.GetMean()+7*self.GetSigma() if not drawRangeMax else drawRangeMax
    # xlow = 0.7
    # xup = 1.3
    self.GetHisto().GetXaxis().SetRangeUser(xlow, xup)
    self.GetHisto().Draw();
    self.DrawExtendedFit();
    self.ResetTextCounters();

    if( self.PrintText ):
      self.PrintFitInfo();

  def ResetTextCounters(self):
    self.lNlines=0
    self.rNlines=0

  ## Print fit information to the canvas ##
  def PrintFitInfo(self):
    self.DrawTextRight(self.fitDesc,self.fitCol);
    self.DrawTextRight( 'chi/dof: {:.2f}'.format(self.GetChi2Ndof()), self.fitCol);
    self.DrawTextRight( "mean: ({:.2f}#pm{:.2f})%".format( (self.GetMean()-1.0)*100, self.GetMeanError()*100), self.fitCol);
    self.DrawTextRight( "width: ({:.2f}#pm{:.2f})%".format( self.GetSigma()*100, self.GetSigmaError()*100), self.fitCol)

  def LogFitInfo(self):
    if not self.fit:
        return
    print( 'chi/dof    : {:.3f}'.format(self.GetChi2Ndof()) )
    print( "mean       : ({:.3f} +/- {:.3f})%".format( self.fit.GetParameter(1), self.fit.GetParError(1) ) )
    print( "width      : ({:.3f} +/- {:.3f})%".format( self.fit.GetParameter(2), self.fit.GetParError(2) ) )
    if self.fitDesc=='Crystal Ball':
      print( "norm       : ({:.3f} +/- {:.3f})%".format( self.fit.GetParameter(0) , self.fit.GetParError(0) ) )
      print( "fraction   : ({:.3f} +/- {:.3f})%".format( self.fit.GetParameter(5) , self.fit.GetParError(5) ) )
      print( "left alpha : ({:.3f} +/- {:.3f})%".format( self.fit.GetParameter(3) , self.fit.GetParError(3) ) )
      print( "right alpha: ({:.3f} +/- {:.3f})%".format( self.fit.GetParameter(6) , self.fit.GetParError(6) ) )
      print( "left n     : ({:.3f} +/- {:.3f})%".format( self.fit.GetParameter(4) , self.fit.GetParError(4) ) )
      print( "right m    : ({:.3f} +/- {:.3f})%".format( self.fit.GetParameter(7) , self.fit.GetParError(7) ) )
    
    
  ## Draw the fit extended past the fit range ##
  def DrawExtendedFit(self, minx = 0, maxx = 3):
    fit = self.GetFit();

    lowX = fit.GetMinimumX()
    highX = fit.GetMaximumX()
    # lowX, highX = ROOT.double(0), ROOT.double(0)
    # fit.GetRange( lowX, highX )
    lineStyle = fit.GetLineStyle()

    fit.SetRange(minx,maxx);
    fit.SetLineStyle(2)
    fit.DrawCopy("same")

    fit.SetRange(lowX, highX)
    fit.SetLineStyle(lineStyle)

    self.GetFit().Draw("same");
    return

  def DrawTextLeft(self, txt, col):
    self.DrawText(self.xLeft,self.yTop-self.dy*(self.lNlines),txt,col);
    self.lNlines += 1
    return

  def DrawTextRight(self, txt, col):
    self.DrawText(self.xRight,self.yTop-self.dy*(self.rNlines),txt,col);
    self.rNlines += 1
    return

  def DrawText(self, x, y, txt, col):
    tex = ROOT.TLatex()
    tex.SetNDC()
    tex.SetTextFont(42)
    tex.SetTextSize(self.textSize)
    tex.SetTextColor(col)
    tex.DrawLatex(x,y,txt)
    return

  def OptimalRebin( self, h ):
    method = 1
    N = h.GetEffectiveEntries();
    if N == 0:
      print("WARNING: Cannot do optimal rebinning due to effective entries being zero!")
      return
    optWidth = 3.5*h.GetRMS()/ROOT.TMath.Power(N,1.0/3);
    Nbins=int(h.GetNbinsX());
    fitRange = h.GetBinLowEdge(Nbins+1) - h.GetBinLowEdge(1);
    rebin=1;
    prevWidth=fitRange/Nbins;
    for i in range(1, Nbins) :
      if (Nbins%i!=0):
        continue;
      binWidth=fitRange/Nbins*i;

      if (method==1):
        if (binWidth<optWidth):
           rebin=i;
      elif (method==2):
        if (ROOT.TMath.Abs(binWidth-optWidth) < ROOT.TMath.Abs(prevWidth-optWidth)):
          rebin=i;
      else:
        rebin=i; #// method 3

      if (binWidth>optWidth):
         break;
      prevWidth=binWidth;

    _vebose=False;
    if (_vebose):
      printf("\n%s\n  RMS: %.3f, Neff: %.3f\n",h.GetName(),h.GetRMS(),N);
      printf("  Opt width: %6.3f, histo binwidth: %6.3f => Rebin: %d\n",optWidth,fitRange/Nbins,rebin);
    self.fitHist.Rebin(rebin);
    return

######  Set attributes of fitter ####
  def SetFitColor(self, col):
    self.fitCol = col
    return

  def SetGaus(self):
    self.param="gaus"
    self.fitDesc="Gaussian"

  def SetPoisson(self):
    self.param="[0]*2.8*TMath::Poisson(x/pow([2],2),[1]/pow([2],2))"
    self.fitDesc="Modified Poisson"

  def SetCrystalBall(self):
    # see https://root.cern/doc/v626/group__PdfFunc.html#gacfd358728d4217061f00c44625b3810e
    # parameter order: alpha, n, sigma, mean
    # if cbsign<0:
    #     self.param="[0]*ROOT::Math::crystalball_pdf(-x, [3], [4], [2], [1])"
    # else:
    #     self.param="[0]*ROOT::Math::crystalball_pdf(x, [3], [4], [2], [1])"
    self.param="[0]*([5]*ROOT::Math::crystalball_pdf(x, [3], [4], [2], [1])+(1-[5])*ROOT::Math::crystalball_pdf(x, [6], [7], [2], [1]))"
    self.fitDesc="Crystal Ball"

  def SetCrystalBallParLimits(self):
    self.fit.SetParLimits(1, self.fitMeanMin, self.fitMeanMax);
    self.fit.SetParLimits(2, self.fitWidthMin, self.fitWidthMax);
    # alpha: 3 and 6
    # sigma: 4 and 7
    # fraction in second CB side: 5
    self.fit.SetParLimits(5,0,1)
    abs_alpha_min = 0.5
    abs_alpha_max = 100
    n_min = 0.01
    n_max = 1000
    self.fit.SetParLimits(3,-abs_alpha_max,-abs_alpha_min)
    self.fit.SetParLimits(4,n_min,n_max)
    self.fit.SetParLimits(6,abs_alpha_min,abs_alpha_max)
    self.fit.SetParLimits(7,n_min,n_max)
    return

  def SetInitialCBParameters(self,histo,fitMin=None,fitMax=None):
    self.fit.SetParameter(0,histo.GetEffectiveEntries()*self.fractionMaxBin)
    self.fit.SetParameter(5,0.75)
    self.fit.SetParameter(3,-1.3)
    self.fit.SetParameter(6,1.3)
    nSigma = 3
    self.fit.SetParameter(4,nSigma)
    self.fit.SetParameter(7,nSigma)
    self.SetCrystalBallParLimits()
    if self.debug:
        self.fit.Print()

  def FitInitialGaussianForCB(self,histo,fitMin=None,fitMax=None):
    f = ROOT.TF1( "fitgaus", "gaus", fitMin, fitMax )    
    if not self.KickBias:
        self.fit.SetParameters( histo.GetMaximum(), histo.GetMean(), histo.GetRMS() );
    else:
        # randomly vary initial mean within 1/10 of bin size to avoid bias when true mean is too close to initial mean
        initmean = ROOT.gRandom.Gaus(histo.GetMean(),0.1*histo.GetBinWidth(histo.FindFirstBinAbove(histo.GetMean())))
        self.fit.SetParameters( histo.GetMaximum(), initmean , histo.GetRMS() );
    f.SetParLimits(1, self.fitMeanMin, self.fitMeanMax);
    f.SetParLimits(2, self.fitWidthMin, self.fitWidthMax);
    fMin = histo.GetMean() - self.Nsigma * histo.GetRMS()
    fMax = histo.GetMean() + self.Nsigma * histo.GetRMS()
    if self.debug:
        print("Fit range: ",fMin,fMax)
    imin = f.GetMinimumX()
    imax = f.GetMaximumX()
    f.SetRange(fMin,fMax)
    histo.Fit(f,self.fitOpt+'R')
    # self.DrawFitAndHisto()
    f.SetRange(imin,imax)
    par1 = f.GetParameter(1)
    par2 = f.GetParameter(2)
    return (par1,par2)

  def FitOneSideCB(self,histo,cbsign:float=1,fitMin=None,fitMax=None):
    ffit = copy.deepcopy(self.fit.Clone())
    if self.debug:
        print("one sided fit with ", cbsign)
    if cbsign<0:
        if self.debug:
            print("Fixing positive side parameters")
        self.fit.SetParameter(0,histo.GetEffectiveEntries()*self.fractionMaxbin)
        self.fit.FixParameter(5,0)
        self.fit.FixParameter(6,10)
        self.fit.FixParameter(7,1)
        # self.fit.FixParameter(6,10)
    else:
        if self.debug:
            print("Fixing negative side parameters")
        self.fit.SetParameter(0,histo.GetEffectiveEntries()*self.fractionMaxbin)
        self.fit.FixParameter(5,1)
        self.fit.FixParameter(3,-10)
        self.fit.FixParameter(4,1)
        # self.fit.FixParameter(4,-10)
    if self.debug:
        self.fit.Print()
    # if cbsign<0:
    #     f = self.Fit(histo,fitMin,1)
    # else:
    #     f = self.Fit(histo,1,fitMax)
    if self.debug:
        print("Before 1-sided CB fit")
        self.LogFitInfo()
    result = histo.Fit(self.fit,self.fitOpt)
    # f = self.Fit(histo,fitMin,fitMax)    
    if self.debug:
        print("After 1-sided CB fit")
        self.LogFitInfo()
    self.fit = ffit
    #self.DrawFitAndHisto()
    # self.SetCrystalBallParLimits() # clear the fixed parameters
    # 4350 => problems
    if result and result.Status()==4000:
        if cbsign<0:
            self.fit.SetParameter(3,f.GetParameter(3))
            self.fit.SetParameter(4,f.GetParameter(4))
        else:
            self.fit.SetParameter(6,f.GetParameter(6))
            self.fit.SetParameter(7,f.GetParameter(7))        
    
    
  def SetRebin( self, doRebin ):
    self.rebin = doRebin
    return

  def SetFitOpt(self, opts):
    self.fitOpt = opts

