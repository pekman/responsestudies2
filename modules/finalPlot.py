import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import modules.superPlot as superPlot
import mplhep as hep
from math import isclose
import sys

from matplotlib.backends.backend_pdf import PdfPages

def xz_given_slice_df(df,current_x,slicing_axis,projection_axis,mode):
    if mode == "eta":
        if current_x<0:
            column_name = "eta_low"
        if current_x>0:
            column_name = "eta_high"
    elif mode == "pt":
        column_name = "pt"
    #print(df[projection_axis].loc[df[column_name].between(current_x-0.1,current_x+0.1)])

    # Returns the x (projection bin) and z (correction factor) for a given slice in the slicing_axis, for a dataframe
    x = df[projection_axis].loc[df[column_name].between(current_x-0.01,current_x+0.01)]
    z = df["correction_factor"].loc[df[column_name].between(current_x-0.01,current_x+0.01)]
    #data_x_error = df[projection_axis+"_error"].loc[(currentSlice[0]<=df[slicing_axis]) & (df[slicing_axis]<=currentSlice[1])]
    #data_z_error = df["z_error"].loc[(currentSlice[0]<=df[slicing_axis]) & (df[slicing_axis]<=currentSlice[1])]
    return x,z

def loop(mode,year,project_path,rootFilePath,list_of_slices,period,config):
    dfPath = f'{project_path}final_calibration/pickles/gaussianKernel_df_{year}'
    df = pd.read_pickle(dfPath+".pickle")
    #print(df["pt"].to_string())
    #sys.exit()

    if mode == "eta":
        slicing_axis="eta"
        projection_axis="pt"
        slice_values=[]
        for current_slice in list_of_slices:
            slice_values.append(round(current_slice[0]+(current_slice[1]-current_slice[0])/2,2))
    elif mode == "pt":
        slicing_axis="pt"
        projection_axis="eta"
        slice_values = [49.987842,102.227080,209.058353,308.845378,400.611575,519.643957,631.600801,719.339429,819.266242,933.074356,1062.692090,1569.932678,]


    figure_width = 30/2.54
    figure_height = 21/2.54
    with PdfPages(f'{project_path}final_calibration/plots/{year}_{mode}_slice_finalCalibration.pdf') as pdf:
        f, ax = plt.subplots(figsize=(figure_width, figure_height))

        color_list = [
                        '#3969acff',
                        '#df3414ff',
                        '#009574ff',
                        '#e68310ff',
                        '#70ad49ff',
                        '#d650adff',
                        ]

        color_list2=color_list.copy()
        color_list2.reverse()
        color_list3 = color_list+color_list2

        for i, current_eta in enumerate(slice_values):
            data_x, data_z = xz_given_slice_df(df,current_eta,slicing_axis,projection_axis,mode)
            if current_eta < 0:
                fill_style="none"
                line_style="dashed"
            elif current_eta > 0:
                fill_style="full"
                line_style="solid"
            
            if mode == "eta":
                label = str(list_of_slices[i])
                color = color_list3[i]
                alpha = 1
            elif mode == "pt":
                label = str(round(current_eta,1))
                color = "#3969acff"
                alpha = 1 - i*0.08

            ax.plot(data_x,data_z,linestyle=line_style,label=label,fillstyle=fill_style, color=color,alpha=alpha)
            #ax.plot(data_x, data_z,marker="o",markersize=3,markeredgewidth=0.1,linewidth=0.1,label=str(list_of_slices[i]),fillstyle=fill_style, color=color_list3[i],alpha=1)
            #ax.plot(data_x, data_z,marker="o",markersize=2,markeredgewidth=0.1,linewidth=0.1,label=str(list_of_slices[i]),fillstyle=fill_style,alpha=1)

        if mode =="eta":
            ax.set_xscale("log")
            ax.set_xlim((50,10000))
            ax.set_xlabel(f"HLT Jet pT", fontsize=14, ha='right', x=1.0)
        elif mode == "pt":
            ax.set_xlim((-3,3))
            ax.set_xlabel(f"HLT Jet eta", fontsize=14, ha='right', x=1.0)

        ax.set_ylim((0.99,1.04))
        ax.grid(which="both")

        
        ax.set_ylabel(f"Correction Factor", fontsize=14, ha='right', y=1.0)

        # Shrink current axis's height by 10% on the bottom
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.03, box.width, box.height * 0.90])
        # Put a legend below current axis
        leg=ax.legend(title=f"{year}, G.Kernel widths: pT({config.gausKernelPtWidths[0]}), eta({config.gausKernelEtaWidths[0]})",loc='upper center', bbox_to_anchor=(0.5, 1.20),
                  fancybox=True, shadow=True, ncol=5)

        # Add ATLAS label
        label=hep.atlas.text("Internal",ax=ax,loc=0,pad=0.05)
        pdf.savefig()
        ax.clear()

def main(year,project_path,rootFilePath,list_of_slices,period,config):
    modes = ["eta","pt"]
    for mode in modes:
        loop(mode,year,project_path,rootFilePath,list_of_slices,period,config)




#    eta_list = []
#    for current_slice in list_of_slices:
#        eta_list.append(round(current_slice[0]+(current_slice[1]-current_slice[0])/2,2))
#    pt_slices = [[50,75],[75,105],[105,160],[160,230],[230,340],[340,500],[500,735],[735,1075],[1075,1580],[1580,2320],[2320,3405],[3405,5000]]
#    pt_list=[]
#    for current_slice in pt_slices:
#        pt_list.append(round(current_slice[0]+(current_slice[1]-current_slice[0])/2,2))

#    print(pt_list)
#    print(eta_list)

#    dfPath = f'{project_path}final_calibration/pickles/gaussianKernel_df_{year}'
#    df = pd.read_pickle(dfPath+".pickle")

#    # eta slice
#    for eta in eta_list:
#        print(find_nearest(df["eta"],eta))

