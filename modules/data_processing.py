import ROOT
import pandas as pd
import numpy as np
import scipy as scipy
from modules.JES_BalanceFitter import JES_BalanceFitter
from modules.JES_CBBalanceFitter import JES_CBBalanceFitter
import pickle
import sys

def initializeDataframe(config, year, period, currentSlice):
    
    # Open the root file
    inFile = ROOT.TFile.Open(config.rootFilePath(year,period))
    if config.verbose: print(f"Initializing dataframe for {year}")
        
    listOfKeys = inFile.GetListOfKeys() # Get the 3D histogram names from the root file

    dictionaryList=[] # Initialize an empty list to house the dictionaries, one per slice

    # Loop over the 3D Histogram names
    for key in listOfKeys:
        TH3Name = key.GetName() # Get the actual name as a string
        #print(TH3Name)
        try:
            current_energy_scale = TH3Name.split("_-_")[1].split("-")[0]
        except IndexError:
            continue
        if TH3Name[0:2] != "h_": # Skip the histograms which are not intended for this
            continue
        elif TH3Name.split("_")[8] != config.xAxisVariable:
            continue
        elif current_energy_scale != config.energy_scale:
            continue
        elif TH3Name.split("_-_")[-1] != config.slicingAxis:
            #print(TH3Name.split("_-_")[-1] , config.slicingAxis)
            continue
        elif inFile.Get(TH3Name).GetEntries()==0.0: # Print a warning if a 3D histogram is empty
            print("WARNING:",TH3Name," is empty!")
            continue
        else: # If everything is good, append an empty pandas series to the dictionary list
            trigger=int(TH3Name.split("_")[2][1:])
            dictionaryList.append(pd.Series({
                                 "trigger":trigger,
                                 "x"             :[],
                                 "x_low_edge"    :[],
                                 "x_high_edge"   :[],
                                 "y"             :[],
                                 "xError"        :[],
                                 "yError"        :[],
                                 "yMean"         :[],
                                 "yMeanError"    :[],
                                 "sigma"         :[],
                                 "sigmaError"    :[],
                                 "sigmaOverY"    :[],
                                 "sigmaOverYError"    :[],
                                 "fitAmplitude"  :[],
                                 "fitMin"        :[],
                                 "fitMax"        :[],
                                 "p_value"       :[],
                                 "Chi2Ndof"      :[],

                                 "TH1BinEdges"   :[],
                                 "TH1BinEntries" :[],
                                 "TH1BinErrors"  :[],
                                 "fitParameters" : [],
                               },
                                  name=TH3Name))

    # Define a path and name for the dataframe based on the slice range
    dfPath = f'{config.project_path}merged/{year}/period{period}/sliceNdice_pickles/{year}_slice[{str(currentSlice[0])},{str(currentSlice[1])}]_{config.xAxisVariable}'
    df = pd.DataFrame(dictionaryList) # Create the dataframe form the dictionary list
    df = df.sort_values("trigger") # Sort the dataframe based on the trigger
    df.to_pickle(dfPath+".pickle") # Save the dataframe to disk as pickle. One dataframe per slice per root file path
    inFile.Close() # Close the root file

def sliceAndDice(config, year, period, currentSlice):
    # Define the dataframe path
    dfPath = f'{config.project_path}merged/{year}/period{period}/sliceNdice_pickles/{year}_slice[{str(currentSlice[0])},{str(currentSlice[1])}]_{config.xAxisVariable}'
    df = pd.read_pickle(dfPath+".pickle") # Read the appropriate dataframe. One per slice, per file path
    year = config.rootFilePath(year,period).split("/")[-1].split("_")[0] # Find which dataset from the root file path
    if config.verbose: print(f"\n{year}\n\tTrigger\tBoundaries (GeV)")
    fpathbase = f'{config.project_path}merged/{year}/period{period}/individual_fits/'+str(currentSlice)+'_'+year+f'_period{period}'
    fpath = fpathbase+'.root'
    hfile_write = ROOT.TFile.Open( fpath, 'RECREATE', 'Individual fits')

    # Define fitting parameters, we don't have to change these
    # But someone will want to in the future
    slicingAxis = "z"
    projectionAxis = "y"
    responseAxis = "x"
    nSigmaForFit = 1.3
    fitOptString = "RESQ"
    
    # Define a dictionary to hold the trigger boundaries that we come up with
    new_boundary_dict={
                        "data15":{},
                        "data16":{},
                        "data17":{},
                        "data18":{},
    }
    
    inFile = ROOT.TFile.Open(config.rootFilePath(year,period)) # Open the root file
    
    # Loop over the differen 3D histogram names in the dataframe
    # If we are dealing with mjj we double all trigger boundaries
    if config.xAxisVariable == "mjj":
        boundary_multiplication_factor = 1
        original_bin_edges = [92,107, 122, 138, 154, 171, 188, 206, 224, 243, 262, 282, 302, 323, 344, 365, 387, 410, 433, 457, 481, 506, 531, 556, 582, 608, 635, 662, 690, 719, 748, 778, 808, 839, 871, 903, 936, 970, 1004, 1039, 1075, 1111, 1148, 1186, 1225, 1264, 1304, 1345, 1387, 1429, 1472, 1516, 1561, 1607, 1654, 1701, 1749, 1798, 1848, 1899, 1951, 2004, 2058, 2113, 2169, 2226, 2284, 2343, 2403, 2464, 2526, 2590, 2655, 2721, 2788, 2856, 2926, 2997, 3069, 3142, 3217, 3293, 3371, 3450, 3530, 3612, 3695, 3780, 3866, 3954, 4043, 4134, 4227, 4321, 4417, 4515, 4614, 4715, 4818, 4923, 5030]
    else:
        boundary_multiplication_factor = 1
        original_bin_edges = list(np.arange(25,2505,5))
        
    for TH3Name in df.index:
        
        # Obtain the pre-determined trigger efficencies
        trigger_efficiency_dict = config.trigger_efficiency_dict_by_year[year]
        
        trigger=int(TH3Name.split("_")[2][1:]) # Find out which trigger we are on
        trigger_list = list(trigger_efficiency_dict.keys()) # Create a list of avaliable triggers
        trigger_list.sort() # Sort the list of triggers

        # Obtain where in the list the current trigger is, its index
        try:
            current_trigger_index = trigger_list.index(trigger)
        except ValueError:
            #The current trigger from the root file is not in the trigger efficiencies list
            continue

        # Calcualte which is the previous trigger, unless the current trigger is the first
        if current_trigger_index == 0:
            previous_trigger = None
            previous_trigger_upper_edge=trigger_efficiency_dict[trigger][0]*boundary_multiplication_factor
        else:
            previous_trigger = trigger_list[current_trigger_index-1]
            previous_trigger_upper_edge =  new_boundary_dict[year]["j"+str(previous_trigger)][1]

        # Calcualte which is the next trigger, unless the current trigger is the last
        if current_trigger_index == len(trigger_list)-1:
            next_trigger = None
            next_trigger_lower_edge = 2500
        else:
            next_trigger = trigger_list[current_trigger_index+1]
            next_trigger_lower_edge = trigger_efficiency_dict[next_trigger][0]*boundary_multiplication_factor

        inTH3 = inFile.Get(TH3Name) # Retreive the current 3D histogram form the root file
        h3D = inTH3.Clone() # Clone it ot keep it intact

        #Set fitting options for the JES_BalanceFitter tool
        JESBfitter = JES_BalanceFitter(config.nSigmaForFit)
        JESBfitter.SetGaus()
        JESBfitter.SetFitOpt(config.fitOptString)
        JESBfitter.rebin = config.optimalRebin

        #Set fitting options for the JES_CBBalanceFitter tool
        JEScbfitter = JES_CBBalanceFitter(config.nSigmaForFit) # fit range for initial core gaussian determination
        JEScbfitter.SetGaus() # SetCrystalBall happens later
        JEScbfitter.SetFitOpt(config.fitOptString)
        JEScbfitter.rebin = config.optimalRebin

        
        # Set the 3D histogram range to correspond to the desired slice range
        if slicingAxis == "y":
            h3D.GetYaxis().SetRangeUser(currentSlice[0], currentSlice[1])     
        elif slicingAxis == "z":
            h3D.GetZaxis().SetRangeUser(currentSlice[0], currentSlice[1])

        # Project the 3D histogram with the sliced axis range into a 2D histogram
        h2D=h3D.Project3D(responseAxis + projectionAxis)

        # Do some fancy re-binnng based on the number of entries in the 1D histogram
        done=False
        broken=False
        left_bin=1
        right_bin=0
        trigger_lowest_edge=False

        if config.verbose:
            print(f"\t{trigger}\t{np.array(trigger_efficiency_dict[trigger])*boundary_multiplication_factor}")

        current_trigger_bins = list(np.array(trigger_efficiency_dict[trigger])*boundary_multiplication_factor)

        new_bin_increment = config.rebinXVar
        
        #print(current_trigger_bins)
        
        for i in range(new_bin_increment,len(current_trigger_bins)//config.rebinXVar):
            # get new bin edges:
            new_left_bin_edge = current_trigger_bins[i-new_bin_increment]
            new_right_bin_edge = current_trigger_bins[i]

            # Get index in old bining
            # FIXME: this can probably be done with "FindBin"
            left_bin_index = original_bin_edges.index(new_left_bin_edge)+1
            right_bin_index = original_bin_edges.index(new_right_bin_edge) # NOTE: I think this is right, as edge is included in the next bin
            #print(left_bin_edge,right_bin_edge)

            # Project the current 2D histogram into a 1D histogram
            h1D=h2D.ProjectionY(f"pt_[{new_left_bin_edge},{new_right_bin_edge}]_eta_{currentSlice}", left_bin_index, right_bin_index)
            # skip empty histograms
            if h1D and abs(h1D.GetEntries())<1e-12: # empty histogram
                continue

            # Check
            actual_low_edge = h2D.GetXaxis().GetBinLowEdge(left_bin_index)
            actual_high_edge = h2D.GetXaxis().GetBinUpEdge(right_bin_index)
            bin_centerr = (actual_low_edge+(actual_high_edge-actual_low_edge)/2)

            # rebin response if desired
            if config.rebinResponse!=1:
                if config.offsetResponseRebins: # offset bins
                    binedges = [h1D.GetBinLowEdge((config.rebinResponse*i)+1+1) for i in range(h1D.GetNbinsX()//config.rebinResponse)]
                    h1D=h1D.Rebin(len(binedges)-1,'',array.array('d',binedges))
                else: # align new bin edges to original histogram edges (standard rebinning)
                    h1D.RebinX(config.rebinResponse)
            
            
            # Set the fit limits based on the 1D histogram properties
            fitMax = h1D.GetMean() + config.nSigmaForFit * h1D.GetRMS()
            fitMin = h1D.GetMean() - config.nSigmaForFit * h1D.GetRMS()

            # Obtain fit using JES_BalanceFitter or JES_CBBalanceFitter

            if config.responseModel=='gaus':
                # Obtain fit using JES_BalanceFitter
                JESBfitter.Fit(h1D, fitMin, fitMax)
                fitter = JESBfitter
            elif config.responseModel=='cb':
                # Obtain fit using JES_CBBalanceFitter
                #ROOT.gStyle.SetOptFit(111111111)
                JEScbfitter.SetCrystalBall()
                JEScbfitter.Fit(h1D, config.responseModelCBMin, config.responseModelCBMax)
                # JEScbfitter.LogFitInfo()
                fitter = JEScbfitter
            else:
                raise ValueError('config.responseModel is not "gaus" or "cb"')
            
            fit = fitter.GetFit()
            histFit = fitter.GetHisto()
            chi2 = fitter.GetChi2()
            ndof = fitter.GetNdof()
            Chi2Ndof = fitter.GetChi2Ndof()

            isBadFit = False
            if config.individualFitPdfs: # save individual fit pdfs
                # histogram_filename = fpathbase + '_j'+str(trigger)+'_'+str(new_left_bin_edge)+'_'+str(new_right_bin_edge)+'.pdf'
                cc = ROOT.TCanvas()
                cc.SetLogy(1)
                ROOT.gStyle.SetOptFit(11111)
                fitter.GetHisto().SetMinimum(0.5)
                if 'Unprescaled' in config.energy_scale:
                    fitter.GetHisto().SetMaximum(35e7)
                else:
                    fitter.GetHisto().SetMaximum(35e5)
                fitter.DrawFitAndHisto(0.5,1.5)
                # separate good and bad fits by name
                if isBadFit:
                    histogram_filename = fpathbase + '_BADFIT_j'+str(trigger)+'_'+str(new_left_bin_edge)+'_'+str(new_right_bin_edge)+'.pdf'
                else:
                    histogram_filename = fpathbase + '_GOODFIT_j'+str(trigger)+'_'+str(new_left_bin_edge)+'_'+str(new_right_bin_edge)+'.pdf'
                cc.Print(histogram_filename)

            effective_entries = histFit.GetEffectiveEntries()
            if effective_entries<100 or ndof==0:
                continue

            p_value = ROOT.TMath.Prob(chi2,ndof)

            if config.inflate_errors and chi2/ndof>1:
                inflation_factor = np.sqrt(chi2/ndof)
            else:
                inflation_factor = 1.0

            hfile_write.WriteObject(histFit,config.xAxisVariable+'_j'+str(trigger)+'_'+str(new_left_bin_edge)+'_'+str(new_right_bin_edge)) #+f'_isBadFit{isBadFit}')
            inFile.cd()

            # Initialize empty lists to hold the values of each 1D histogram.
            # There will be as many 1D histograms as Y axis bins of the 3D histogram
            binEdges=[]
            binEntries=[]
            binErrors=[]

            # Loop over the response bins in the 1D histogram
            for i in range(1, h1D.GetNbinsX()+1):# Plus one to include last bin
              binEdges.append(h1D.GetXaxis().GetBinLowEdge(i)) # Get the current bin edge for plotting later
              binEntries.append(h1D.GetBinContent(i)) # Get the number of entries in the bin for plotting later
              binErrors.append(h1D.GetBinError(i)) # Get the bin error to get the width of the bin... For plotting later
            binEdges.append(h1D.GetXaxis().GetBinUpEdge(h1D.GetNbinsX()))# Append the right most edge of the last bin

            # Append all the lists for this 1D histogram to the list of lists in the dataframe.
            # One dataframe per slice, per root file path

            # Append all the values of the 1D histogram gaussian fit to the lists of the current data frame series
            # There are ~4 series per data frame, 1 dataframe per slice, per root file path

            df["x"].loc[TH3Name].append(actual_low_edge+(actual_high_edge-actual_low_edge)/2)
            df["x_low_edge"].loc[TH3Name].append(actual_low_edge)
            df["x_high_edge"].loc[TH3Name].append(actual_high_edge)
            df["y"].loc[TH3Name].append(float(fit.GetParameter(1)))
            df["xError"].loc[TH3Name].append(float((actual_high_edge-actual_low_edge)/2))#half bin width
            df["yError"].loc[TH3Name].append(float(fit.GetParError(1))*inflation_factor)
            df["yMean"].loc[TH3Name].append(float(h1D.GetMean()))
            df["yMeanError"].loc[TH3Name].append(float(h1D.GetRMS()/scipy.sqrt(h1D.Integral())))
            df["sigma"].loc[TH3Name].append(float(fit.GetParameter(2)))
            df["sigmaError"].loc[TH3Name].append(float(fit.GetParError(2))*inflation_factor)
            df["p_value"].loc[TH3Name].append(float(p_value))
            df["Chi2Ndof"].loc[TH3Name].append(float(Chi2Ndof))
            try:
                df["sigmaOverY"].loc[TH3Name].append(float(fit.GetParameter(2) / float(fit.GetParameter(1))))
                df["sigmaOverYError"].loc[TH3Name].append(np.sqrt((fit.GetParError(2)/fit.GetParameter(2))**2+(fit.GetParError(1)/fit.GetParameter(1))**2))
            except:
                df["sigmaOverY"].loc[TH3Name].append(0)
                df["sigmaOverYError"].loc[TH3Name].append(0)
            # Append the lists of 1D histogram info to the list of lists in the dataframe data series
            df["TH1BinEdges"].loc[TH3Name].append(binEdges)
            df["TH1BinEntries"].loc[TH3Name].append(binEntries)
            df["TH1BinErrors"].loc[TH3Name].append(binErrors)
            df["fitParameters"].loc[TH3Name].append([fit.GetParameter(0),fit.GetParameter(1),fit.GetParameter(2), fitMin, fitMax, Chi2Ndof,fitter.FitResult.Prob(),fitter.FitResult.IsValid()])
            df["name"]=TH3Name
            df["trigger"]= int(TH3Name.split("_")[2][1:])
            df["energyScale"]= TH3Name.split("_")[4].split("-")[0]
            df["xVariable"]= TH3Name.split("_")[8]
            df["zVariable"]= TH3Name.split("_")[10]
            df["year"]= year

        new_boundary_dict[year]["j"+str(trigger)]=[current_trigger_bins[0],current_trigger_bins[-1]]

    # Save the daraframe and the trigger boundary dict that we determined
    df.to_pickle(dfPath+".pickle") # Save the dataframe to disk as pickle. One dataframe per slice per root file path
    boundary_path= f'{config.project_path}merged/{year}/period{period}/boundary_dictionaries/{year}_slice[{str(currentSlice[0])},{str(currentSlice[1])}]_{config.xAxisVariable}'
    with open(boundary_path+".pickle", 'wb') as handle:
        pickle.dump(new_boundary_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)
    inFile.Close()
    hfile_write.Close()

