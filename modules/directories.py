import os

def new_project(name,recreate=False,configUpdateDict=None):
    '''create new configuration python for a project.

    if recreate=True, overwrite any project settings already existing with that
    name; otherwise, throws a FileExistsError exception.

    if the option configUpdateDict is provided, each configuration parameter
    appearing in the dictionary (as a key) is updated to the specified value
    (the value associated with the key).

    '''
    project_path= f'projects/{name}/'
    data_path = 'data/cow/'
    years=["data16","data17","data18"]
    os.makedirs(f"{project_path}",exist_ok=recreate)
    os.makedirs(f"{project_path}/merged",exist_ok=recreate)
    os.makedirs(f"{project_path}/final_calibration",exist_ok=recreate)
    os.makedirs(f"{project_path}/final_calibration/plots",exist_ok=recreate)
    os.makedirs(f"{project_path}/final_calibration/pickles",exist_ok=recreate)
    for year in years:
        os.makedirs(f"{project_path}/merged/{year}",exist_ok=recreate)
        os.makedirs(f"{project_path}/merged/{year}/periodX",exist_ok=recreate)
        os.makedirs(f"{project_path}/merged/{year}/periodX/boundary_dictionaries",exist_ok=recreate)
        os.makedirs(f"{project_path}/merged/{year}/periodX/stitched_response_dataframes",exist_ok=recreate)
        os.makedirs(f"{project_path}/merged/{year}/periodX/sliceNdice_pickles",exist_ok=recreate)
        os.makedirs(f"{project_path}/merged/{year}/periodX/response_plots",exist_ok=recreate)
        os.makedirs(f"{project_path}/merged/{year}/periodX/individual_fits",exist_ok=recreate)
        os.makedirs(f"{project_path}/merged/{year}/periodX/calibration_output",exist_ok=recreate)
        os.makedirs(f"{project_path}/merged/{year}/periodX/calibration_output/plots",exist_ok=recreate)
        os.makedirs(f"{project_path}/merged/{year}/periodX/calibration_output/pickles",exist_ok=recreate)
        os.makedirs(f"{project_path}/merged/{year}/periodX/calibration_output/calibration_material",exist_ok=recreate)

    with open("modules/config.py",mode='rt') as file:
        config_template = file.read()
        config_template_lines = config_template.split('\n')
        config_template_lines = [l for l in config_template_lines if not l.startswith('#')] # remove unindented comments
        # update values if requested
        if configUpdateDict:
            for (par,value) in configUpdateDict.items():
                #   enclose string values in quotes first.
                v = value
                if type(value) is str:
                    v = f"'{value}'"
                # find line in the initialization to update. there's probably a
                # more clever way to do this.
                for (i,ln) in enumerate(config_template_lines):
                    if par in ln:
                        ln = f'        self.{par} = {v};'
                        config_template_lines[i] = ln
                        break
                
        # build the final configuration template (rejoin the list of lines) 
        config_template = '\n'.join(config_template_lines)

    default_config='''
# this is a copy of modules/config.py. edit project settings and data paths here.
{config_template}
config = Configuration()
config.data_path = '{data_path}' # path to the input data
config.project_path = '{project_path}' # path to this config and its outputs
'''.format(config_template=config_template,
           data_path=data_path,
           project_path=project_path)

    
    mode = 'xt'
    if recreate: mode = 'wt'
    with open(f"{project_path}/{name}_config.py",mode=mode) as file:
        file.write(default_config)
