import pandas as pd
import ROOT
import numpy as np
import sys
import os
import matplotlib.pyplot as plt
import mplhep as hep
import array
import uproot

from matplotlib.backends.backend_pdf import PdfPages


def get_profiles_as_df(rootFilePath):
    profile_dict = {
        "hhProfile_ptProbe_ptAvg_etaRef": {
            "probe_pT": [],
            "offline_pt_low": [],
            "offline_pt_high": [],
            "offline_eta_low": [],
            "offline_eta_high": [],
        },
        "hhProfile_etaProbe_ptAvg_etaRef": {
            "probe_eta": [],
            "offline_pt_low": [],
            "offline_pt_high": [],
            "offline_eta_low": [],
            "offline_eta_high": [],
        },
    }
    inFile = ROOT.TFile.Open(rootFilePath)
    for key in profile_dict:
        TProfileName = key
        inTProfileName = inFile.Get(
            TProfileName
        )  # Retreive the current 3D histogram form the root file
        TProfile = inTProfileName.Clone()  # Clone it ot keep it intact

        original_bin_edges = list(np.arange(25, 2505, 5))
        log_bin_edges = [
            25,
            30,
            35,
            40,
            45,
            50,
            55,
            60,
            70,
            75,
            80,
            90,
            100,
            110,
            120,
            130,
            145,
            155,
            170,
            190,
            205,
            230,
            250,
            275,
            300,
            330,
            360,
            395,
            435,
            475,
            520,
            570,
            625,
            690,
            755,
            825,
            905,
            995,
            1090,
            1195,
            1310,
            1440,
            1575,
            1730,
            1895,
            2080,
            2280,
            2500,
        ]

        for y in range(0, TProfile.GetNbinsY() + 1):
            y_low_edge = TProfile.GetYaxis().GetBinLowEdge(y)
            y_high_edge = TProfile.GetYaxis().GetBinUpEdge(y)

            for i in range(1, len(log_bin_edges)):
                left_bin_edge = log_bin_edges[i - 1]
                right_bin_edge = log_bin_edges[i]
                left_bin = original_bin_edges.index(left_bin_edge)
                right_bin = (
                    original_bin_edges.index(right_bin_edge) - 1
                )  # NOTE: I think this is right, as edge is included in the next bin

                sum_w = 0
                sum_wx = 0
                for j in range(left_bin, right_bin + 1):
                    probe_value = TProfile.GetBinContent(j, y)
                    probe_error = TProfile.GetBinError(j, y)
                    if probe_error > 0.0:
                        w = 1 / probe_error / probe_error
                        sum_w += w
                        sum_wx += w * probe_value
                if sum_w > 0.0:
                    probe_weighted_average = sum_wx / sum_w
                else:
                    probe_weighted_average = 0.0  # FIXME: Make nan instead of 0

                profile_dict[key]["offline_pt_low"].append(left_bin_edge)
                profile_dict[key]["offline_pt_high"].append(right_bin_edge)
                profile_dict[key]["offline_eta_low"].append(y_low_edge)
                profile_dict[key]["offline_eta_high"].append(y_high_edge)
                if key == "hhProfile_ptProbe_ptAvg_etaRef":
                    profile_dict[key]["probe_pT"].append(probe_weighted_average)
                elif key == "hhProfile_etaProbe_ptAvg_etaRef":
                    profile_dict[key]["probe_eta"].append(probe_weighted_average)

    pt_profile = pd.DataFrame(profile_dict["hhProfile_ptProbe_ptAvg_etaRef"])
    eta_profile = pd.DataFrame(profile_dict["hhProfile_etaProbe_ptAvg_etaRef"])

    return pt_profile, eta_profile


def get_probe_pt(offline_pt_avg, offline_pt_eta_low, offline_pt_eta_high, pt_profile):
    to_print = pt_profile.loc[
        (pt_profile["offline_pt_low"] <= offline_pt_avg)
        & (offline_pt_avg <= pt_profile["offline_pt_high"])
        & (round(pt_profile["offline_eta_low"], 1) == offline_pt_eta_low)
        & (offline_pt_eta_high == round(pt_profile["offline_eta_high"], 1))
    ]
    probe_pT = to_print["probe_pT"].iloc[0]
    return probe_pT


def get_probe_eta(offline_pt_avg, offline_pt_eta_low, offline_pt_eta_high, eta_profile):
    to_print = eta_profile.loc[
        (eta_profile["offline_pt_low"] <= offline_pt_avg)
        & (offline_pt_avg <= eta_profile["offline_pt_high"])
        & (round(eta_profile["offline_eta_low"], 1) == offline_pt_eta_low)
        & (offline_pt_eta_high == round(eta_profile["offline_eta_high"], 1))
    ]
    probe_eta = to_print["probe_eta"].iloc[0]
    return probe_eta


def get_df(config, year, period):

    # Make one dataframe using the stitched trigger response for all the different slices
    list_of_dataframes = []
    for currentSlice in config.list_of_slices:
        dfPath = f"{config.project_path}merged/{year}/period{period}/stitched_response_dataframes/{year}_slice[{str(currentSlice[0])},{str(currentSlice[1])}]_{config.xAxisVariable}"
        list_of_dataframes.append(pd.read_pickle(dfPath + ".pickle"))
    df = pd.concat(list_of_dataframes, ignore_index=True)
    pt_profile, eta_profile = get_profiles_as_df(config.rootFilePath(year, period))

    # Get the numerically inverted probe pT and eta
    df["probe_pT"] = "None"
    df["probe_eta"] = "None"
    for index, row in df.iterrows():
        offline_pt_avg = row["offline_pt_avg"]
        offline_pt_eta_low = row["offline_eta_low"]
        offline_pt_eta_high = row["offline_eta_high"]
        probe_pT = get_probe_pt(
            offline_pt_avg, offline_pt_eta_low, offline_pt_eta_high, pt_profile
        )
        probe_eta = get_probe_eta(
            offline_pt_avg, offline_pt_eta_low, offline_pt_eta_high, eta_profile
        )
        df.at[index, "probe_pT"] = probe_pT
        df.at[index, "probe_eta"] = probe_eta

    # Get pt Bin edges
    df["offline_pt_low"] = df["offline_pt_avg"] - df["offline_pt_avg_error"]
    df["offline_pt_high"] = df["offline_pt_avg"] + df["offline_pt_avg_error"]
    return df


def df_to_Graph2D(df, config, year, period):
    fpath = f"{config.project_path}merged/{year}/period{period}/calibration_output/calibration_material/{year}_period{period}_2DGraph.root"
    hfile = ROOT.TFile.Open(fpath, "RECREATE", "Demo ROOT file with histograms")
    hfile.cd()

    df_graph = df
    df_graph["probe_pt_error"] = df["offline_pt_avg_error"]
    df_graph["probe_eta_error"] = (df["offline_eta_high"] - df["offline_eta_low"]) / 2
    df_graph["percentage_error"] = df["response_error"] / df["response"]
    df_graph["correction_factor_error"] = (
        df_graph["percentage_error"] * df_graph["correction_factor"]
    )

    # FIXME this is hardcoded
    # df_graph = df_graph.loc[df_graph["correction_factor_error"]<0.0025]
    df_graph = df_graph.reset_index()
    # print(df_graph)

    number_of_entries = len(df_graph.index)
    graph = ROOT.TGraph2DErrors(number_of_entries)
    for index, row in df_graph.iterrows():
        # print(index)
        graph.SetPoint(
            index, row["probe_pT"], row["probe_eta"], row["correction_factor"]
        )
        graph.SetPointError(
            index,
            row["probe_pt_error"],
            row["probe_eta_error"],
            row["correction_factor_error"],
        )

    graph.Write()
    hfile.Close()


def do_cpp_gaussian_kernel_fitting(config, year, period, ptWidth, etaWidth):
    in_file = f"{config.project_path}merged/{year}/period{period}/calibration_output/calibration_material/{year}_period{period}_2DGraph.root"
    out_file = f"{config.project_path}merged/{year}/period{period}/calibration_output/calibration_material/{year}_Calibration_TH2D.root"
    command = (
        "root -l -x "
        + "'./modules/alex.cxx"
        + f'("{in_file}","{out_file}",{ptWidth},{etaWidth})'
        + "'"
    )
    os.system(command)


def dag_plot(df, config, year, period):
    df["offline_eta_avg"] = df["offline_eta_low"] + (
        (df["offline_eta_high"] - df["offline_eta_low"]) / 2
    )
    df["offline_pt_avg2"] = df["offline_pt_low"] + (
        (df["offline_pt_high"] - df["offline_pt_low"]) / 2
    )
    pd.set_option("display.max_rows", 11)
    # sys.exit()

    f, ax1 = plt.subplots(figsize=(8 * 2.5 * (1 / 2.54), 6 * 2.5 * (1 / 2.54)))
    ax1.plot(
        df["offline_pt_avg2"],
        df["offline_eta_avg"],
        linestyle="None",
        marker="o",
        markersize=3,
        label="Offline Bin Center",
        color="orange",
    )
    ax1.plot(
        df["probe_pT"],
        df["probe_eta"],
        linestyle="None",
        marker="o",
        markersize=1,
        label="HLT Average",
        color="black",
    )

    indexx = 216
    print(df["response"][indexx], df["correction_factor"][indexx])
    print(f'offline:{df["offline_pt_avg2"][indexx]}, {df["offline_eta_avg"][indexx]}')
    print(f'online:{df["probe_pT"][indexx]}, {df["probe_eta"][indexx]}')

    # ax1.plot(df["offline_pt_avg2"][215], df["offline_eta_avg"][215], linestyle='None', marker="o",markersize=3, label=f'Offline: {df["offline_pt_avg2"][215]}, {df["offline_eta_avg"][215]}', color="blue")
    # ax1.plot(df["probe_pT"][215], df["probe_eta"][215], linestyle='None', marker="o",markersize=1, label=f'Offline: {round(df["probe_pT"][215],1)}, {round(df["probe_eta"][215],1)}', color="red")

    ax1.set_xscale("log")
    leg1 = ax1.legend(
        borderpad=0.5,
        loc=1,
        ncol=1,
        frameon=True,
        facecolor="white",
        framealpha=1,
        fontsize="medium",
    )
    leg1._legend_box.align = "left"
    ax1.set_xlim(50, 5000)
    ax1.set_ylim(-2.5, 2.5)
    ax1.set_ylabel("Offline Eta", fontsize=14, ha="right", y=1.0)
    ax1.set_xlabel("Offline Avg. $p_T$", fontsize=14, ha="right", x=1.0)

    # Add ATLAS label
    label = hep.atlas.text("Internal", ax=ax1, loc=0, pad=0.03)

    # f.savefig(f'{config.project_path}merged/{year}/period{period}/calibration_output/plots/{year}_dagPlot.pdf')
    f.savefig(f"{config.project_path}final_calibration/plots/{year}_dagPlot.pdf")


def p_value_distribution(df, config, year, period):
    f, ax1 = plt.subplots(figsize=(2 * 25 * (1 / 2.54), 2 * 13.875 * (1 / 2.54)))
    ax1.hist(df["p_value"], bins=200, range=(0, 1))
    ax1.set_xlabel("p-value", fontsize=18, ha="right", x=1.0)
    ax1.set_ylabel("Entries", fontsize=18, ha="right", y=1.0)
    ax1.set_title("Distribution of p-values from individual response fits", fontsize=22)
    f.savefig(
        f"{config.project_path}merged/{year}/period{period}/calibration_output/plots/{year}_pValue.pdf"
    )


def freeze_calibration(config, df):
    dict_freeze_points = {
        "eta_low": [],
        "maxPT_probe_eta": [],
        "eta_high": [],
        "min_pt": [],
        "max_pt": [],
        "minPT_correction_factor": [],
        "maxPT_correction_factor": [],
    }
    for current_slice in config.list_of_slices:
        this_series = df.loc[
            df["probe_eta"].between(current_slice[0], current_slice[1])
        ]
        dict_freeze_points["eta_low"].append(current_slice[0])
        dict_freeze_points["maxPT_probe_eta"].append(this_series["probe_eta"].iloc[-1])
        dict_freeze_points["eta_high"].append(current_slice[1])
        dict_freeze_points["min_pt"].append(this_series["probe_pT"].iloc[0])
        dict_freeze_points["max_pt"].append(this_series["probe_pT"].iloc[-1])
        dict_freeze_points["minPT_correction_factor"].append(
            this_series["correction_factor"].iloc[0]
        )
        dict_freeze_points["maxPT_correction_factor"].append(
            this_series["correction_factor"].iloc[1]
        )

        # dict_freeze_points['correction_factor'].append(this_series["minPT_correction_factor"].idxmin())
    df_freeze_points = pd.DataFrame(dict_freeze_points)
    df_freeze_points["energy"] = df_freeze_points["max_pt"] * np.cosh(
        df_freeze_points["maxPT_probe_eta"]
    )
    return df_freeze_points


def get_new_factor(row):
    if row["energy"] < row["energy_limit"]:
        correction_factor = row["correction_factor"]
        last_approved_correction_factor = row["correction_factor"]
    elif row["energy"] > row["energy_limit"]:
        correction_factor = last_approved_correction_factor
    return correction_factor


def freeze_2d_hist(year, period, config, df, highest_lowest_pt):
    outFile = ROOT.TFile(
        f"{config.project_path}merged/{year}/period{period}/calibration_output/calibration_material/{year}_Calibration_TH2D_frozen.root",
        "recreate",
    )
    inFile = ROOT.TFile.Open(
        f"{config.project_path}merged/{year}/period{period}/calibration_output/calibration_material/{year}_Calibration_TH2D.root"
    )

    highest_lowest_pt = 80  # FIXME

    h_in = inFile.Get("calibration")
    h_out = h_in.Clone()

    h2d_dict = {
        "eta": [],
        "pt": [],
        "correction_factor": [],
        "energy": [],
        "energy_limit": [],
    }

    for y in range(1, h_out.GetNbinsY() + 1):
        eta = h_out.GetYaxis().GetBinCenter(y)
        for x in range(1, h_out.GetNbinsX() + 1):
            pt = h_out.GetXaxis().GetBinCenter(x)
            h2d_dict["eta"].append(eta)
            h2d_dict["pt"].append(pt)
            h2d_dict["correction_factor"].append(h_out.GetBinContent(x, y))
            h2d_dict["energy"].append(pt * np.cosh(eta))
            row = df.loc[(df["eta_low"] < eta) & (eta < df["eta_high"])].values
            h2d_dict["energy_limit"].append(row[0][7])

    h2d_df = pd.DataFrame(h2d_dict)

    h2d_df["new_correction_factor"] = h2d_df["correction_factor"]

    for index, row in h2d_df.iterrows():
        if row["energy"] < row["energy_limit"]:
            correction_factor = row["correction_factor"]
            last_approved_correction_factor = row["correction_factor"]
        elif row["energy"] > row["energy_limit"]:
            correction_factor = last_approved_correction_factor
        h2d_df["new_correction_factor"].iloc[index] = correction_factor
    ######################################################################

    #    h2d_df=h2d_df.sort_values(by='pt', ascending=False)
    #    for index, row in h2d_df.iterrows():
    #        if row["pt"]>row["pt_limit"]:
    #            correction_factor = row["correction_factor"]
    #            last_approved_correction_factor = row["correction_factor"]
    #        elif row["pt"]<row["pt_limit"]:
    #            correction_factor = last_approved_correction_factor
    #        h2d_df["new_correction_factor"].iloc[index]=correction_factor

    #    h2d_df=h2d_df.sort_values(by='pt', ascending=True)
    #    print(h2d_df)

    #    list_of_df_etas = list(h2d_df["eta"])
    #    h2d_df_sorted = h2d_df.sort_values(by='pt', ascending=False)
    #    for eta in list_of_df_etas:
    #        for point in lowest_point_list:
    #            eta_low = point[0]
    #            eta_high = point[1]
    #            if (eta_low <= eta  and  eta <= eta_high):
    #                lowest_pt=point[2]
    #                break
    #        temp = h2d_df_sorted.loc[h2d_df_sorted["eta"]==eta]
    #        for index, row in temp.iterrows():
    #            print(row["pt"])
    #            if row["pt"]>lowest_pt:
    #                correction_factor = row["correction_factor"]
    #                last_approved_correction_factor = row["correction_factor"]
    #            elif row["pt"]<=lowest_pt:
    #                correction_factor = last_approved_correction_factor
    #            h2d_df_sorted["new_correction_factor"].iloc[index]=correction_factor
    #    h2d_df=h2d_df_sorted
    ######################################################################

    #    for point in lowest_point_list:
    #        eta_low = point[0]
    #        eta_high = point[1]
    #        lowest_pt=point[2]

    #    h2d_df["lowest_pt"]=-1
    #    for point in lowest_point_list:
    #        eta_low = point[0]
    #        eta_high = point[1]
    #        lowest_pt=point[2]
    #        for index, row in h2d_df.iterrows():
    #            if (eta_low <= row["eta"]  and  row["eta"] <= eta_high):
    #                h2d_df["lowest_pt"].iloc[index]=lowest_pt
    #
    #    h2d_df["pt_diff"]=abs(h2d_df["pt"]-h2d_df["lowest_pt"])
    #
    #
    #    print(h2d_df.groupby("eta").min())

    #    h2d_df_sorted = h2d_df.sort_values(by='pt', ascending=False)
    # print(h2d_df_sorted)
    #    for point in lowest_point_list:
    #        eta_low = point[0]
    #        eta_high = point[1]
    #        lowest_pt=point[2]
    #        h2d_df_sorted = h2d_df_sorted.loc[h2d_df_sorted["eta"].between(eta_low,eta_high)]
    #        h2d_df_sorted = h2d_df_sorted.apply(lambda product: lowest_pt if x.pt < else x.pt, axis=1)
    #        print(h2d_df_sorted)
    #
    #        break

    #        for index, row in h2d_df.iterrows():
    #            #print(f'{eta_low}<={row["eta"]} and {row["eta"]}<={eta_high}')
    #            #print(   eta_low <= row["eta"]  and  row["eta"] <= eta_high)
    #            if (eta_low <= row["eta"]  and  row["eta"] <= eta_high):
    #                #print(f'{eta_low}<={row["eta"]} and {row["eta"]}<={eta_high}')
    #                if row["pt"] >= lowest_pt:
    #                    last_approved_correction_factor = row["correction_factor"]
    #                elif row["pt"] < lowest_pt:
    #                    correction_factor = last_approved_correction_factor
    #                h2d_df_sorted["new_correction_factor"].iloc[index]=correction_factor
    #        h2d_df = h2d_df_sorted

    #            if (eta_low<=row["eta"] and row["eta"]<=eta_high):
    #                print(eta_low<=row["eta"] and row["eta"]<=eta_high,eta_low,eta,eta_high)
    #                print(eta_low<=row["eta"] and row["eta"]<=eta_high,type(eta_low),type(eta),type(eta_high))
    #            else:
    #                continue

    #    for point in lowest_point_list:
    #        h2d_df_sorted = h2d_df[h2d_df['eta'].between(point[0], point[1])]
    #        h2d_df_sorted = h2d_df_sorted.sort_values(by='pt', ascending=False)
    #        lowest_pt=point[2]+100
    #        for index, row in h2d_df_sorted.iterrows():
    #            if row["pt"]>lowest_pt:
    #                correction_factor = row["correction_factor"]
    #                last_approved_correction_factor = row["correction_factor"]
    #            elif row["pt"]<lowest_pt:
    #                correction_factor = last_approved_correction_factor
    #            h2d_df["new_correction_factor"].iloc[index]=correction_factor
    #        print(point[0], point[1],last_approved_correction_factor)

    for y in range(h_out.GetNbinsY(), 0, -1):
        eta = h_out.GetYaxis().GetBinCenter(y)
        for x in range(h_out.GetNbinsX(), 0, -1):
            pt = h_out.GetXaxis().GetBinCenter(x)
            if pt >= highest_lowest_pt:
                row = (
                    h2d_df["new_correction_factor"]
                    .loc[(h2d_df["eta"] == eta) & (h2d_df["pt"] == pt)]
                    .values
                )
                adjusted_corr = row[0]
            elif pt < highest_lowest_pt:
                pass
            current_bin = h_out.GetBin(x, y)
            h_out.SetBinContent(current_bin, adjusted_corr)

    outFile.cd()
    h_out.Write()
    outFile.Close()
    inFile.Close()


def freeze_2d_hist_below_85(df, config):
    df_sorted = df.sort_values(by="probe_pT", ascending=False)
    lowest_point_list = []
    for eta in config.list_of_slices:
        series = df.loc[
            df_sorted["offline_eta_low"] == eta[0]
        ]  # & df_sorted["offline_eta_high"]==eta[1]
        lowest_point_list.append(series.iloc[0]["probe_pT"])
    highest_lowest_pt = max(lowest_point_list)
    print("highest_lowest_pt= ", highest_lowest_pt)
    return highest_lowest_pt


def get_bins(df, low, high):
    seet = list(set(df[low]))
    seet.sort()
    seet.append(np.amax(df[high]))
    bins = np.array(seet)
    return bins


#
#    df["offline_pt_low"] = df["offline_pt_avg"]-df["offline_pt_avg_error"]
#    df["offline_pt_high"] = df["offline_pt_avg"]+df["offline_pt_avg_error"]
#    seet = list(set(df["offline_pt_low"]))
#    seet.sort()
#    #seet.append(np.amax(df["offline_pt_high"]))
#    pt_bins=seet


def TH2_to_hist2d(df, h2d, fig, ax, variable, title_dict, bins, title, xlabel, ylabel):
    x = []
    y = []
    w = []
    for index, row in df.iterrows():
        h2d.Fill(row["offline_pt_avg"], row["offline_eta_avg"], row[f"{variable}"])
        x.append(row["offline_pt_avg"])
        y.append(row["offline_eta_avg"])
        w.append(row[f"{variable}"])

    hist = ax.hist2d(
        x, y, weights=w, bins=bins, cmin=0.00000000000000000000000000000001
    )
    ax.set_title(title_dict[variable])
    ax.set_ylabel("Eta", ha="right", y=1.0)
    ax.set_xlabel("pT [GeV]", ha="right", x=1.0)
    ax.set_xscale("log")
    fig.colorbar(hist[3], ax=ax)


def df_to_TH2(df, variable, title_dict):
    eta_bins = get_bins(df, "offline_eta_low", "offline_eta_high")
    df["offline_pt_low"] = df["offline_pt_avg"] - df["offline_pt_avg_error"]
    df["offline_pt_high"] = df["offline_pt_avg"] + df["offline_pt_avg_error"]
    pt_bins = get_bins(df, "offline_pt_low", "offline_pt_high")
    pt_bins = pt_bins[:-1]

    df["offline_eta_avg"] = df["offline_eta_low"] + (
        (df["offline_eta_high"] - df["offline_eta_low"]) / 2
    )
    df["offline_pt_avg2"] = df["offline_pt_low"] + (
        (df["offline_pt_high"] - df["offline_pt_low"]) / 2
    )

    h2d = ROOT.TH2F(
        f"{title_dict[variable]}",
        f"{title_dict[variable]}",
        len(pt_bins) - 1,
        pt_bins,
        len(eta_bins) - 1,
        eta_bins,
    )
    return h2d, pt_bins, eta_bins


def fit_performance_plot(df, config, year, period):

    variables = "response_error", "p_value", "diff", "chi", "sigma", "sigma_error"

    title_dict = {
        "response_error": "Error on mean from fit",
        "p_value": "p-value of fit",
        "diff": "Difference Between Average and Mean of Fit",
        "chi": "Chi^2/ndf",
        "sigma": "Standard deviation of fit",
        "sigma_error": "Error of Standard deviation of fit",
    }

    limit_dict = {
        "response_error": 0.01,
        "p_value": 1.0,
        "diff": 0.025,
        "chi": 20,
        "sigma": 0.08,
        "sigma_error": 0.01,
    }
    current_path = f"{config.project_path}/merged/{year}/periodX/individual_fits/"
    for variable in variables:
        print(variable)
        h2d, x_bins, y_bins = df_to_TH2(df, variable, title_dict)
        with PdfPages(current_path + f"{variable}.pdf") as pdf:
            for slicee in config.list_of_slices:
                print(slicee)
                inFile = ROOT.TFile.Open(
                    current_path + f"[{slicee[0]}, {slicee[1]}]_{year}_periodX.root"
                )
                listOfKeys = (
                    inFile.GetListOfKeys()
                )  # Get the 3D histogram names from the root file
                for key in listOfKeys:
                    TH3Name = key.GetName()
                    eta_low = slicee[0]
                    eta_high = slicee[1]
                    pt_low = float(TH3Name.split("_")[-2])
                    pt_high = float(TH3Name.split("_")[-1])

                    fig, axs = plt.subplots(1, 2, figsize=(8 * 2, 6), dpi=300)
                    TH2_to_hist2d(
                        df,
                        h2d,
                        fig,
                        axs[0],
                        variable,
                        title_dict,
                        [x_bins, y_bins],
                        variable,
                        "pT",
                        "Eta",
                    )
                    axs[0].plot(
                        [pt_low, pt_low, pt_high, pt_high, pt_low],
                        [eta_low, eta_high, eta_high, eta_low, eta_low],
                        marker=None,
                        color="red",
                        zorder=100,
                    )

                    th1 = inFile.Get(TH3Name)

                    # Initialize empty lists to hold the values of each 1D histogram.
                    # There will be as many 1D histograms as Y axis bins of the 3D histogram
                    binCenter = []
                    binWidth = []
                    binErrors = []
                    binContent = []
                    # Loop over the response bins in the 1D histogram
                    for i in range(
                        1, th1.GetNbinsX() + 1
                    ):  # Plus one to include last bin
                        binCenter.append(th1.GetXaxis().GetBinCenter(i))
                        binWidth.append(th1.GetXaxis().GetBinWidth(i))
                        binErrors.append(th1.GetBinError(i))
                        binContent.append(th1.GetBinContent(i))
                    axs[1].errorbar(
                        binCenter,
                        binContent,
                        xerr=binWidth,
                        yerr=binErrors,
                        linestyle="None",
                    )

                    parameters = th1.GetFunction("fit").GetParameters()
                    p0 = parameters[0]
                    p1 = parameters[1]
                    p2 = parameters[2]
                    # fitMin = df["gaussianParameters"].loc[TH3Name][i][3]
                    # fitMax = df["gaussianParameters"].loc[TH3Name][i][4]
                    # Chi2Ndof = df["gaussianParameters"].loc[TH3Name][i][5]

                    x_fit = np.linspace(0, 2, 1000)
                    y_fit = p0 * np.exp(-0.5 * ((x_fit - p1) / p2) ** 2)

                    axs[1].plot(x_fit, y_fit, label="Fit", color="red")

                    axs[1].set_title(
                        f"{year}\neta: [{slicee[0]}, {slicee[1]}], pt: [{pt_low}, {pt_high}]"
                    )
                    axs[1].set_ylabel("entries", ha="right", y=1.0)
                    axs[1].set_xlabel("Response", ha="right", x=1.0)

                    plt.tight_layout()
                    pdf.savefig()
                    plt.close(fig)

                inFile.Close()
                # sys.exit()


#    for variable in variables:
#        h2d, x_bins, y_bins = df_to_TH2(df,variable,title_dict)
#
#        fig, axs = plt.subplots(1,2,figsize=(8*2,6),dpi=300)
#        TH2_to_hist2d(df,h2d,fig,axs[0],variable,[x_bins, y_bins],variable,"pT","Eta")
#        plt.tight_layout()
#        plt.savefig(f"./variables/{year}/{variable}.png")
#
#        sys.exit()
# fig, ax = plt.subplots(1,1,figsize=(8,6),dpi=600)


# TH2_to_hist2d()


#        c1 = ROOT.TCanvas( 'c1', f'{variable}', 200, 10, 700, 500 )
#        h2d.Draw("colz")
#        h2d.SetStats(0)
#        h2d.GetXaxis().SetTitle("pT [GeV]")
#        h2d.GetYaxis().SetTitle("Eta")
#        h2d.SetMaximum(limit_dict[variable])
#        c1.cd()
#        c1.Update()
#        c1.SaveAs(f"./variables/{year}/{variable}2.png")
#        c1.Close()
#        sys.exit()

#    f, ax1 = plt.subplots(figsize=(8*2.5*(1/2.54), 6*2.5*(1/2.54)))
#    #ax1.plot(df["offline_pt_avg2"], df["offline_eta_avg"], linestyle='None', marker="o",markersize=3, label='Offline Bin Center', color="orange")
#    IFE_ratio = np.array(df["chi"])
#    IFE_ratio = np.reshape(IFE_ratio, (len(pt_bins), len(eta_bins)))
#    ax1.pcolormesh(pt_bins, eta_bins, IFE_ratio.T)
#    ax1.set_xscale('log')
#    leg1 = ax1.legend(borderpad=0.5, loc=1, ncol=1, frameon=True,facecolor="white",framealpha=1,fontsize='medium')
#    leg1._legend_box.align = "left"
#    ax1.set_xlim(50,5000)
#    ax1.set_ylim(-2.5,2.5)
#    ax1.set_ylabel("Offline Eta", fontsize=14, ha='right', y=1.0)
#    ax1.set_xlabel("Offline Avg. $p_T$", fontsize=14, ha='right',x=1.0)
#    label=hep.atlas.text("Internal",ax=ax1,loc=0,pad=0.03)
#    f.savefig(f'{config.project_path}final_calibration/plots/{year}_test.pdf')

# def fit_performance_plot(df,config,year,period):

#    seet = list(set(df["offline_eta_low"]))
#    seet.sort()
#    seet.append(np.amax(df["offline_eta_high"]))
#    eta_bins=array.array('d',seet)
#
#    df["offline_pt_low"] = df["offline_pt_avg"]-df["offline_pt_avg_error"]
#    df["offline_pt_high"] = df["offline_pt_avg"]+df["offline_pt_avg_error"]
#    seet = list(set(df["offline_pt_low"]))
#    seet.sort()
#    #seet.append(np.amax(df["offline_pt_high"]))
#    pt_bins=seet
#
##    eta_bins = array.array('d',[-2.5,-1.8,-1.3,-1,-0.7,-0.2,0,0.2,0.7,1,1.3,1.8,2.5])
#    if config.xAxisVariable == "mjj":
#        boundary_multiplication_factor = 1
#        original_bin_edges = array.array('d',[92,107, 122, 138, 154, 171, 188, 206, 224, 243, 262, 282, 302, 323, 344, 365, 387, 410, 433, 457, 481, 506, 531, 556, 582, 608, 635, 662, 690, 719, 748, 778, 808, 839, 871, 903, 936, 970, 1004, 1039, 1075, 1111, 1148, 1186, 1225, 1264, 1304, 1345, 1387, 1429, 1472, 1516, 1561, 1607, 1654, 1701, 1749, 1798, 1848, 1899, 1951, 2004, 2058, 2113, 2169, 2226, 2284, 2343, 2403, 2464, 2526, 2590, 2655, 2721, 2788, 2856, 2926, 2997, 3069, 3142, 3217, 3293, 3371, 3450, 3530, 3612, 3695, 3780, 3866, 3954, 4043, 4134, 4227, 4321, 4417, 4515, 4614, 4715, 4818, 4923, 5030])
#    else:
#        boundary_multiplication_factor = 1
#        original_bin_edges = array.array('d',pt_bins)
##        #original_bin_edges = list(np.arange(25,2505,5))
##        original_bin_edges =array.array('d',[25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 160, 165, 170, 175, 180, 185, 190, 195, 200, 205, 210, 215, 220, 225, 230, 235, 240, 245, 250, 255, 260, 265, 270, 275, 280, 285, 290, 295, 300, 305, 310, 315, 320, 325, 330, 335, 340, 345, 350, 355, 360, 365, 370, 375, 380, 385, 390, 395, 400, 405, 410, 415, 420, 425, 430, 435, 440, 445, 450, 455, 460, 465, 470, 475, 480, 485, 490, 495, 500, 505, 510, 515, 520, 525, 530, 535, 540, 545, 550, 555, 560, 565, 570, 575, 580, 585, 590, 595, 600, 605, 610, 615, 620, 625, 630, 635, 640, 645, 650, 655, 660, 665, 670, 675, 680, 685, 690, 695, 700, 705, 710, 715, 720, 725, 730, 735, 740, 745, 750, 755, 760, 765, 770, 775, 780, 785, 790, 795, 800, 805, 810, 815, 820, 825, 830, 835, 840, 845, 850, 855, 860, 865, 870, 875, 880, 885, 890, 895, 900, 905, 910, 915, 920, 925, 930, 935, 940, 945, 950, 955, 960, 965, 970, 975, 980, 985, 990, 995, 1000, 1005, 1010, 1015, 1020, 1025, 1030, 1035, 1040, 1045, 1050, 1055, 1060, 1065, 1070, 1075, 1080, 1085, 1090, 1095, 1100, 1105, 1110, 1115, 1120, 1125, 1130, 1135, 1140, 1145, 1150, 1155, 1160, 1165, 1170, 1175, 1180, 1185, 1190, 1195, 1200, 1205, 1210, 1215, 1220, 1225, 1230, 1235, 1240, 1245, 1250, 1255, 1260, 1265, 1270, 1275, 1280, 1285, 1290, 1295, 1300, 1305, 1310, 1315, 1320, 1325, 1330, 1335, 1340, 1345, 1350, 1355, 1360, 1365, 1370, 1375, 1380, 1385, 1390, 1395, 1400, 1405, 1410, 1415, 1420, 1425, 1430, 1435, 1440, 1445, 1450, 1455, 1460, 1465, 1470, 1475, 1480, 1485, 1490, 1495, 1500, 1505, 1510, 1515, 1520, 1525, 1530, 1535, 1540, 1545, 1550, 1555, 1560, 1565, 1570, 1575, 1580, 1585, 1590, 1595, 1600, 1605, 1610, 1615, 1620, 1625, 1630, 1635, 1640, 1645, 1650, 1655, 1660, 1665, 1670, 1675, 1680, 1685, 1690, 1695, 1700, 1705, 1710, 1715, 1720, 1725, 1730, 1735, 1740, 1745, 1750, 1755, 1760, 1765, 1770, 1775, 1780, 1785, 1790, 1795, 1800, 1805, 1810, 1815, 1820, 1825, 1830, 1835, 1840, 1845, 1850, 1855, 1860, 1865, 1870, 1875, 1880, 1885, 1890, 1895, 1900, 1905, 1910, 1915, 1920, 1925, 1930, 1935, 1940, 1945, 1950, 1955, 1960, 1965, 1970, 1975, 1980, 1985, 1990, 1995, 2000, 2005, 2010, 2015, 2020, 2025, 2030, 2035, 2040, 2045, 2050, 2055, 2060, 2065, 2070, 2075, 2080, 2085, 2090, 2095, 2100, 2105, 2110, 2115, 2120, 2125, 2130, 2135, 2140, 2145, 2150, 2155, 2160, 2165, 2170, 2175, 2180, 2185, 2190, 2195, 2200, 2205, 2210, 2215, 2220, 2225, 2230, 2235, 2240, 2245, 2250, 2255, 2260, 2265, 2270, 2275, 2280, 2285, 2290, 2295, 2300, 2305, 2310, 2315, 2320, 2325, 2330, 2335, 2340, 2345, 2350, 2355, 2360, 2365, 2370, 2375, 2380, 2385, 2390, 2395, 2400, 2405, 2410, 2415, 2420, 2425, 2430, 2435, 2440, 2445, 2450, 2455, 2460, 2465, 2470, 2475, 2480, 2485, 2490, 2495, 2500])

#    variables = 'response_error','p_value', 'diff', 'chi', 'sigma', 'sigma_error'
#
#    h2d = ROOT.TH2F("test","test", len(original_bin_edges)-1,original_bin_edges,len(eta_bins)-1,eta_bins)

#    c1 = ROOT.TCanvas( 'c1', 'Dynamic Filling Example', 200, 10, 700, 500 )

#    print(df.columns)
#    print(len(pt_bins)*len(eta_bins))
#    for index, row in df.iterrows():
#        eta_center = row["offline_eta_low"]+0.001
#        #if row["offline_eta_low"]<0:
#        #    eta_center = row["offline_eta_low"]+(row["offline_eta_low"]+row["offline_eta_high"])/2
#        #else:
#        #    eta_center = row["offline_eta_low"]+(row["offline_eta_high"]-row["offline_eta_low"])/2
#        h2d.Fill(row["offline_pt_avg"],eta_center,row["chi"])

#    h2d.Draw()
#    c1.Update()
#    c1.SaveAs("test.pdf")
#    plt.plot()


def main(config, year, period, ptWidth, etaWidth):
    # Get the numerically inversion correction factor for all slices and stitched triggers as one dataframe
    df = get_df(config, year, period)

    # This function quit calibraiton in a weird way
    # fit_performance_plot(df,config,year,period)

    # sys.exit()

    dag_plot(df, config, year, period)

    # Save the dataframe
    dfPath = (
        f"{config.project_path}final_calibration/pickles/numericalInversion_df_{year}"
    )
    df.to_pickle(dfPath + ".pickle")

    # Convert the dataframe to a TGraph2DErrors object
    df_to_Graph2D(df, config, year, period)

    # Fit that 2D graph with a 2D gaussian kernel, using an external root C++ macro
    do_cpp_gaussian_kernel_fitting(config, year, period, ptWidth, etaWidth)

    energy_limits = freeze_calibration(config, df)

    highest_lowest_pt = freeze_2d_hist_below_85(df, config)

    freeze_2d_hist(year, period, config, energy_limits, highest_lowest_pt)

    p_value_distribution(df, config, year, period)
