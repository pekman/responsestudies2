import ROOT
import uproot
import numpy as np

def main(project_path,listOfYears,period):
    # Gathers all the 2D calibration histograms and puts them in one file

    files=[]
    for year in listOfYears:
        files.append(f'{project_path}merged/{year}/period{period}/calibration_output/calibration_material/{year}_Calibration_TH2D_frozen.root')

    histoName = "calibration"
    output = ROOT.TFile(f'{project_path}final_calibration/TLA_OnlineToOffline_Correction_July2022.root', "recreate")
    for f in files:
        year=f.split("/")[-1].split("_")[0]

        tfile = ROOT.TFile.Open(f)
        h = tfile.Get(histoName)
        newName = year+"_OnlineToOffline"
        h.SetNameTitle(newName, newName)
        h.SetDirectory(0)
        tfile.Close()

        for y in range(1,h.GetNbinsY()+1):
            for x in range(1,h.GetNbinsX()+1):
                bin_content = h.GetBinContent(x,y)
                assert bin_content != 0.0
        output.cd()
        h.Write()

    output.Close()
