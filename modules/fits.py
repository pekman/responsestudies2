import numpy as np

#### Five-five

def five_five(x,A,B,C,D,F,G,H,J,K,L,pt0):
    #pt0=2070
    E=-(5*A*np.log(pt0)**4) - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5) + (5*L)/(np.log(pt0)**6)
    return A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4) + L*np.log(x)**(-5)



def alpha(x,A,B,C,D,E):
    x1=85
    x0=2000
    D=(4*A*x0**3 + 3*B*x0**2 - 4*A*x1**2*x0 + 3*B*x1*x0)/(1+x1/x0)
    return A*x**4 + B*x**3 + C*x**2 + D*x + E

def original(x,a5,a4,a3,a2,a1,am1,am2,am3,am4,am5,a0):
    return a5*np.log(x)**5 + a4*np.log(x)**4 + a3*np.log(x)**3 + a2*np.log(x)**2 + a1*np.log(x)**1 + a0*np.log(x)**0 + am1*np.log(x)**(-1) + am2*np.log(x)**(-2) + am3*np.log(x)**(-3) + am4*np.log(x)**(-4) + am5*np.log(x)**(-5)

#def beta(x,K,A):
#    return  K + A*np.log(x)**-3
    
#def gamma(x,K,A):
#    B=-(A*np.log(x))/2
#    return  K + A*np.log(x)**-1 + B*np.log(x)**-2

def seven_seven(x,v,a,A,B,C,D,F,G,H,J,K,L,b,w):
    E=-(7*v*np.log(pt0)**6) -(6*a*np.log(pt0)**5) -(5*A*np.log(pt0)**4) - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5) + (5*L)/(np.log(pt0)**6) + (6*b)/(np.log(pt0)**7) + (7*w)/(np.log(pt0)**8)
    return v*np.log(x)**7 + a*np.log(x)**6 + A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4) + L*np.log(x)**(-5) + b*np.log(x)**(-6) + w*np.log(x)**(-7)

def six_six(x,a,A,B,C,D,F,G,H,J,K,L,b):
    E=-(6*a*np.log(pt0)**5) -(5*A*np.log(pt0)**4) - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5) + (5*L)/(np.log(pt0)**6) + (6*b)/(np.log(pt0)**7)
    return a*np.log(x)**6 + A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4) + L*np.log(x)**(-5) + b*np.log(x)**(-6)

def five_six(x,A,B,C,D,F,G,H,J,K,L,b):
    E=-(5*A*np.log(pt0)**4) - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5) + (5*L)/(np.log(pt0)**6) + (6*b)/(np.log(pt0)**7)
    return A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4) + L*np.log(x)**(-5) + b*np.log(x)**(-6)

def four_six(x,B,C,D,F,G,H,J,K,L,b):
    E= - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5) + (5*L)/(np.log(pt0)**6) + (6*b)/(np.log(pt0)**7)
    return B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4) + L*np.log(x)**(-5) + b*np.log(x)**(-6)

def three_six(x,C,D,F,G,H,J,K,L,b):
    E= - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5) + (5*L)/(np.log(pt0)**6) + (6*b)/(np.log(pt0)**7)
    return C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4) + L*np.log(x)**(-5) + b*np.log(x)**(-6)

def two_six(x,D,F,G,H,J,K,L,b):
    E= -(2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5) + (5*L)/(np.log(pt0)**6) + (6*b)/(np.log(pt0)**7)
    return D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4) + L*np.log(x)**(-5) + b*np.log(x)**(-6)

def one_six(x,F,G,H,J,K,L,b):
    E= G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5) + (5*L)/(np.log(pt0)**6) + (6*b)/(np.log(pt0)**7)
    return E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4) + L*np.log(x)**(-5) + b*np.log(x)**(-6)

def six_five(x,a,A,B,C,D,F,G,H,J,K,L):
    E=-(6*a*np.log(pt0)**5) -(5*A*np.log(pt0)**4) - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5) + (5*L)/(np.log(pt0)**6)
    return a*np.log(x)**6 + A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4) + L*np.log(x)**(-5)

def six_four(x,a,A,B,C,D,F,G,H,J,K):
    E=-(6*a*np.log(pt0)**5) -(5*A*np.log(pt0)**4) - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5)
    return a*np.log(x)**6 + A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4)

def six_three(x,a,A,B,C,D,F,G,H,J):
    E=-(6*a*np.log(pt0)**5) -(5*A*np.log(pt0)**4) - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4)
    return a*np.log(x)**6 + A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3)

def six_two(x,a,A,B,C,D,F,G,H):
    E=-(6*a*np.log(pt0)**5) -(5*A*np.log(pt0)**4) - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3)
    return a*np.log(x)**6 + A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2)

def six_one(x,a,A,B,C,D,F,G):
    E=-(6*a*np.log(pt0)**5) -(5*A*np.log(pt0)**4) - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2)
    return a*np.log(x)**6 + A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1)


def five_five_v2(x,A,B,C,D,F,G,H,J,K,L):
    D=-5*A*np.log(pt1)**4-4*B*np.log(pt1)**2-3*C*np.log(pt1)-E*np.log(pt1)**-1+G*np.log(pt1)**-3+2*H*np.log(pt1)**-4+3*J*np.log(pt1)**-5+4*k*np.log(pt1)**-6
    E=-(5*A*np.log(pt0)**4) - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5) + (5*L)/(np.log(pt0)**6)
    return A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4) + L*np.log(x)**(-5)

def five_five_v3(x,A,B,C,D,F,G,H,J,K,L):
    P=pt0
    Q=pt1
    print(P,Q)
    E=(-5*A*np.log(P)**4 -4*B*np.log(P)**3 -3*C*np.log(P)**2 +G*np.log(P)**-2 + 2*H*np.log(P)**-3 +3*J*np.log(P)**5 + 4*K*np.log(P)**5 + 5*L*np.log(P)**6 +10*A*np.log(Q)**4*np.log(P) +8*B*np.log(Q)**2*np.log(P) +6*C*np.log(Q)*np.log(P) -2*G*np.log(P)*np.log(Q)**-3 -4*H*np.log(P)*np.log(Q)**-4 -6*J*np.log(P)*np.log(Q)**-5 -8*K*np.log(P)*np.log(Q)**-6)/(1-(2*np.log(P))/(np.log(Q)))    
    return A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4) + L*np.log(x)**(-5)

### end

def five_four(x,A,B,C,D,F,G,H,J,K):
    E=-(5*A*np.log(pt0)**4) - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5)
    return A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4)

def five_three(x,A,B,C,D,F,G,H,J):
    E=-(5*A*np.log(pt0)**4) - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4)
    return A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3)

def five_two(x,A,B,C,D,F,G,H):
    E=-(5*A*np.log(pt0)**4) - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3)
    return A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2)

def five_one(x,A,B,C,D,F,G):
    E=-(5*A*np.log(pt0)**4) - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2)
    return A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1)

def five_zero(x,A,B,C,D,F):
    E=-(5*A*np.log(pt0)**4) - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0))
    return A*np.log(x)**5 + B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0

def four_five(x,B,C,D,F,G,H,J,K,L):
    E= - (4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5) + (5*L)/(np.log(pt0)**6)
    return B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4) + L*np.log(x)**(-5)

def three_five(x,C,D,F,G,H,J,K,L):
    E= - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5) + (5*L)/(np.log(pt0)**6)
    return C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4) + L*np.log(x)**(-5)

def two_five(x,D,F,G,H,J,K,L):
    E= - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5) + (5*L)/(np.log(pt0)**6)
    return D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4) + L*np.log(x)**(-5)

def one_five(x,D,F,G,H,J,K,L):
    E=  G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5) + (5*L)/(np.log(pt0)**6)
    return E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4) + L*np.log(x)**(-5)

def four_four(x,B,C,D,F,G,H,J,K):
    E = -(4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5)
    return B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4)

def four_three(x,B,C,D,F,G,H,J):
    E = -(4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4)
    return B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3)

def four_two(x,B,C,D,F,G,H):
    E = -(4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3)
    return B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2)

def four_one(x,B,C,D,F,G):
    E = -(4*B*np.log(pt0)**3) - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2)
    return B*np.log(x)**4 + C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1)

def three_four(x,C,D,F,G,H,J,K):
    E = - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5)
    return C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4)

def two_four(x,D,F,G,H,J,K):
    E = - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5)
    return D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4)

def one_four(x,F,G,H,J,K):
    E = G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4) + (4*K)/(np.log(pt0)**5)
    return E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3) + K*np.log(x)**(-4)

def three_three(x,C,D,F,G,H,J):
    E = - (3*C*np.log(pt0)**2) - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3) + (3*J)/(np.log(pt0)**4)
    return C*np.log(x)**3 + D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2) + J*np.log(x)**(-3)

def two_two(x,D,F,G,H):
    E =  - (2*D*np.log(pt0)) + G/(np.log(pt0)**2) + (2*H)/(np.log(pt0)**3)
    return D*np.log(x)**2 + E*np.log(x)**1 + F*np.log(x)**0 + G*np.log(x)**(-1) + H*np.log(x)**(-2)
