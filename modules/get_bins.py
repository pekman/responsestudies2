import numpy as np
#round up to multiple of 5 from both end
def round_to_multiple(number, multiple):
    return multiple * round(number/multiple)

def main():
    my_list=[]
    print("{",end="")
    for i in range(0,51):
        print(f"{round(-2.5+0.1*i,1)}, ",end="")
        my_list.append(round(-2.5+0.1*i,1))
    print("}",end="\n")
#    print(len(my_list))

#    minJetPt = 50
#    maxJetPt = 5000;
#    nPtBins = 12;
#    PtBinEdges=[]
#    for i in range(0,nPtBins+1):
#        PtBinEdges.append(int(np.exp(np.log(minJetPt)+(np.log(maxJetPt)-np.log(minJetPt))*i/(nPtBins))))
#    rounded=[]
#    for entry in PtBinEdges:
#        rounded.append(round_to_multiple(entry,5))
#    rounded = [*set(rounded)]
#    rounded.sort()
#    #(rounded)

    i=1
    print("[",end="")
    while i < len(my_list):
        print(f'[{my_list[i-1]},{my_list[i]}],',end="")
        i+=1
    print("]",end="")

if __name__ == "__main__":
    main()
