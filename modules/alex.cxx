#include "JES_Smoother.C"
#include <string>

// Based on the improved macor based on initial macro from Fabrizio

void alex(string in_file, string out_file, float ptWidth, float etaWidth){

    //if(year==2016):

    JES_Smoother sm;
    // can turn on and off use of bin-uncertainty
    // Here all bins have same uncertainty, so will not matter
    sm.UseBinUncertainty(true);
    sm.SetLog(true,false);
    sm.SetKernelWidth(ptWidth,etaWidth);
    std::vector<double> bins;
    for (int i=0;i<=100;++i){
        bins.push_back(-2.5+0.05*i);
        //cout<<-2.5+0.025*i<<endl;
    }
    sm.SetEtaBins(bins);

    string in_file_path=in_file;
    TFile *f = TFile::Open(in_file_path.c_str());
    TGraph2DErrors * gr = (TGraph2DErrors*)f->Get("Graph2D");
    auto error_list = gr->GetEZ();

    auto h_out = sm.SmoothPtEtaGraph(gr,"calibration");
    h_out->GetXaxis()->SetRangeUser(50,5000);
    h_out->GetZaxis()->SetRangeUser(0.95,1.03);
    //h_out->Draw("surf2");
    h_out->GetXaxis()->SetTitle("HLT_pT");
    h_out->GetYaxis()->SetTitle("HLT_eta");
    string out_file_path=out_file;
    TFile *outFile = TFile::Open(out_file_path.c_str(), "RECREATE");
    outFile->cd();
    h_out->Write();
    gr->Write();
    TGraph2D *g2 = new TGraph2D(h_out);
    g2->Write();
    
    outFile->Close();

    gApplication->Terminate();

}
