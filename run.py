import importlib

import argparse
from argparse import RawTextHelpFormatter

import modules.data_processing as data_processing
import modules.plotting as plotting
import modules.calibration as calibration
import modules.superPlot as superPlot
import modules.combine as combine
import modules.fits as fits
import modules.directories as directories
import modules.finalPlot as finalPlot
from matplotlib.backends.backend_pdf import PdfPages

# import modules.config as configuration
import sys
import ROOT

def get_arguments():
    parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter,
                    prog = "run.py",
                    description =   '''
* Run.py takes care of the looping, looping over years and eta slices\n
* mode=newProject together with projectName=nominal creates the directory\n
  structure for a new project called "nominal".
* mode=process runs data_processing.py per year, period, and eta slice.\n
  Here the 3D-histograms from the input file are sliced and projected into\n
  1D response histograms, which are fitted. The mean of the fit, bin sizes etc\n
  are then saved in dataframes into pickles.
* mode=plot plots the response for each trigger using the data frames created\n
  it also most importantly stitches together the response of the different triggers\n
  and allows us to see how well this stitching worked. The function then saves the\n
  combined response as a dictionary pickle.
* mode=calib takes the combined response values together with 2D TProfiles form the\n
  input file and numerically inverts olffine pt/eta to HLT pt/eta. It also runs the \n
  2D-Gaussian Kernel fitting macro and saves the resulting 2D calibration histograms.\n
* mode=combine takes the 2D calibration histograms created and combines them into one\n
  file which will be given to the JetCalibTools.
* mode=runAll runs all of the above in sequence.
''',
                    epilog = 'Enjoy!')
    parser.add_argument('--config', type=str, required=False, help='Path to project')
    parser.add_argument('--projectName', type=str, required=False, help='Name of new project')
    parser.add_argument('--mode', type=str, required=True, help='process, plot, calib, combine, runAll')
    args = parser.parse_args()
    return args.config, args.mode, args.projectName

def control(mode, config):

    # Gathers all the 2D calibration histograms and puts them in one file
    if mode=="combine":
        period="X"
        combine.main(config.project_path,config.listOfYears,period)
    elif mode=="process" or mode=="plot":
        for year in config.listOfYears:
            period_list = config.period_dict[year]
            for period in period_list:
                #with PdfPages(f'{config.project_path}merged/{year}/period{period}/response_plots/{year}_{config.energy_scale}_responseCollection.pdf') as pdf:
                with PdfPages(f'{config.project_path}merged/{year}/period{period}/response_plots/{year}_slice[-2.5,2.5]_{config.xAxisVariable}_{config.energy_scale}.pdf') as pdf:
                    for currentSlice in config.list_of_slices:
                        print(currentSlice)
                        rootFilePath = config.rootFilePath(year=year,period=period)

                        if mode=="process":
                            # Initializes a dataframe for each year, slice, and trigger
                            data_processing.initializeDataframe(config,year,period,currentSlice)
                            # For each 3D histogram it slices and projects in eta, and prjects 1D histograms in pT
                            # It also produces dataframes with the mean of the fitted 1D histograms
                            data_processing.sliceAndDice(config,year,period,currentSlice)
                        elif mode=="plot":
                            # Stitches together all the different trigger dataframes and plots the response
                            current_fit = fits.five_five
                            plotting.plot(config,year,period,currentSlice,current_fit,pdf)
    elif mode=="calib":
      for year in config.listOfYears:
        if config.verbose:print(year)
        period="X"        
        # Space for looping ove different widths
        for ptWidth in config.gausKernelPtWidths:
            for etaWidth in config.gausKernelEtaWidths:
                # Performs numerical inverstion from offline pt/eta to probe pt/eta on the stitcked together dataframes
                # Also creates 2D graph and fits it with a 2D Gaussian kernel which creatse the calibration 2D histogram
                calibration.main(config,year,period,ptWidth,etaWidth)
    elif mode=="superPlot":
        for year in config.listOfYears:
            if config.verbose:print(year)
            period="X"
            # Plots the numerically inverted correction factors, the 2D Gaussian kernel fit, and poly log fits in all possible slices
            superPlot.main(config,year,period)
    elif mode=="finalPlot":
        print("finalPlot")
        for year in config.listOfYears:
            if config.verbose:print(year)
            period = config.period_dict[year][0]
            rootFilePath = config.data_path+f"merged/{year}/period{period}/{year}_13TeV_period{period}.root"
            # Plots the numerically inverted correction factors, the 2D Gaussian kernel fit, and poly log fits in all possible slices
            finalPlot.main(year,config.project_path,rootFilePath,config.list_of_slices,period,config)


def main():

    # turn on ROOT batch mode (any ROOT graphics can conflict with matplotlib when this routine is called)
    # see e.g. https://root-forum.cern.ch/t/pyroot-properly-releasing-memory-ending-this-script/49724/4
    ROOT.gROOT.SetBatch(True);

    # Get run arguments
    config_path, mode, projectName = get_arguments()
    if mode == "newProject":
        name = projectName
        if name == None:
            print("No project name given, add one with '--projectName'")
        else:
            directories.new_project(name)
    else:
        config_path = config_path
        project_path="/".join(config_path.split("/")[:-1])+"/"

        # Import variables and dictionaries from config
        module_string=config_path.replace(".py","").replace("/",".")
        config_module = importlib.import_module(module_string)
        config = config_module.config


    # Run the different modules based on the running mode

    if mode == "newProject":
        pass
    elif mode != "runAll":
        control(mode, config)
    else:
        list_of_modes = ["process","plot","calib","superPlot", "finalPlot","combine"]
        for mode in list_of_modes:
            control(mode, config)

if __name__ == "__main__":
    main()
