#!/usr/bin/env python3

import importlib

import argparse
from argparse import RawTextHelpFormatter

import modules.data_processing as data_processing
import modules.plotting as plotting
import modules.calibration as calibration
import modules.superPlot as superPlot
import modules.combine as combine
import modules.fits as fits
import modules.directories as directories
# import modules.config as configuration
import sys
import os
import ROOT
import run as driver

from multiprocessing import Pool

# set up configurations
################################################################

def setup_configurations():

    configurations = {}

    #### nominal gaussian (for reference)
    configurations[ 'gaus_nominal' ] = {}
    # - 2x response binning
    configurations[ 'gaus_2xR' ] = { 'rebinResponse' : 2 }
    # - 4x response binning
    configurations[ 'gaus_4xR' ] = { 'rebinResponse' : 4 }
    # - 8x response binning
    configurations[ 'gaus_8xR' ] = { 'rebinResponse' : 8 }
    # - optimal rebin false
    configurations[ 'gaus_NoOptRebin' ] = { 'optimalRebin' : False }
    configurations[ 'gaus_NoOptRebin_2xR' ] = { 'optimalRebin' : False , 'rebinResponse' : 2}
    configurations[ 'gaus_NoOptRebin_8xR' ] = { 'optimalRebin' : False , 'rebinResponse' : 8}
    # - 2x pt binning
    configurations[ 'gaus_2xPt' ] = { 'rebinXVar' : 2 }
    # - 4x pt binning
    configurations[ 'gaus_4xPt' ] = { 'rebinXVar' : 4 }
    # - gaus core / initial range 1.3->1.1,1.5
    configurations[ 'gaus_core1p1' ] = { 'nSigmaForFit' : 1.1 }
    configurations[ 'gaus_core1p5' ] = { 'nSigmaForFit' : 1.5 }

    #### crystal ball

    configurations[ 'cb_nominal' ] = { 'responseModel': 'cb' }
    # - 2x response binning
    configurations[ 'cb_2xR' ] = { 'responseModel': 'cb', 'rebinResponse' : 2 }
    # - 4x response binning
    configurations[ 'cb_4xR' ] = { 'responseModel': 'cb', 'rebinResponse' : 4 }
    # - 8x response binning
    configurations[ 'cb_8xR' ] = { 'responseModel': 'cb', 'rebinResponse' : 8 }
    # - optimal rebin false
    configurations[ 'cb_NoOptRebin' ] = { 'responseModel': 'cb', 'optimalRebin' : False }
    configurations[ 'cb_NoOptRebin_2xR' ] = { 'responseModel': 'cb', 'optimalRebin' : False , 'rebinResponse': 2 }
    configurations[ 'cb_NoOptRebin_4xR' ] = { 'responseModel': 'cb', 'optimalRebin' : False , 'rebinResponse': 4 }
    configurations[ 'cb_NoOptRebin_8xR' ] = { 'responseModel': 'cb', 'optimalRebin' : False , 'rebinResponse': 8 }
    # - 2x pt binning
    configurations[ 'cb_2xPt' ] = { 'responseModel': 'cb', 'rebinXVar' : 2 }
    # - 4x pt binning
    configurations[ 'cb_4xPt' ] = { 'responseModel': 'cb', 'rebinXVar' : 4 }
    # - gaus core / initial range 1.3->1.1,1.5
    configurations[ 'cb_core1p1' ] = { 'responseModel': 'cb', 'nSigmaForFit' : 1.1 }
    configurations[ 'cb_core1p5' ] = { 'responseModel': 'cb', 'nSigmaForFit' : 1.5 }
    # - crystal ball range 0.7,1.3 -> 0.65,1.35
    configurations[ 'cb_cbrange0p65to1p35' ] = { 'responseModel': 'cb', 'responseModelCBMin' : 0.65 , 'responseModelCBMax': 1.35 }

    return configurations

# configure each project
################################################################

def configure_project(name,settings):
    directories.new_project(name,recreate=True,configUpdateDict=settings)


# run each project
################################################################

def run_driver_os(config_path):
    project_path="/".join(config_path.split("/")[:-1])+"/"
    os.system(f'python3 run.py --config={config_path} --mode=runAll >& {project_path}/run.log')

# put the steps together

if __name__ == '__main__':
    #freeze_support()

    configurations = setup_configurations()
    for (name,settings) in configurations.items():
        configure_project(name,settings)
            
    if True: # parallelize via just launching a bunch of os.system subprocesses

        configs = []
        for (name,settings) in configurations.items():
            config_path = f'projects/{name}/{name}_config.py'
            configs.append(config_path)

        max_processes = min([8,len(configs)]) # don't launch more processes than the number of configurations
        pool = Pool(processes=max_processes)
        result = pool.map(run_driver_os, configs)

    else: # sequential processing---do each configuration and step sequentially

        # turn on ROOT batch mode (any ROOT graphics can conflict with matplotlib when this routine is called)
        # see e.g. https://root-forum.cern.ch/t/pyroot-properly-releasing-memory-ending-this-script/49724/4
        ROOT.gROOT.SetBatch(True);
        list_of_modes = ["process","plot","calib","superPlot","combine"]
        for (name,settings) in configurations.items():
            config_path = f'projects/{name}/{name}_config.py'
            project_path="/".join(config_path.split("/")[:-1])+"/"
            # Import variables and dictionaries from config
            module_string=config_path.replace(".py","").replace("/",".")
            config_module = importlib.import_module(module_string)
            config = config_module.config
            for mode in list_of_modes:
                driver.control(mode,config)
