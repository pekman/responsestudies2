# Initial run

```console
git clone https://gitlab.cern.ch/pekman/responsestudies2.git
```

```console
cd responsestudies2
```

```console
rm -r data/ projects/
```

```console
scp -r pekman@lxplus.cern.ch:/eos/user/p/pekman/to_tobias/responsestudies2/data_and_projects.zip ./
```

```console
unzip data_and_projects.zip
```

```console
rm data_and_projects.zip
```

```console
python3 run.py --config projects/flatN90_J100/flatN90_J100_config.py --mode runAll
```

# Good to Know
* You can run the different steps individually by exchanging `--mode runAll` with any of: `process`, `plot`, `calib`, `superPlot`, `finalPlot`, `combine`
* In the configuration file, `projects/flatN90_J100/flatN90_J100_config.py`, you can change which year you are running, the eta slices, the energy scales included, gaussian or CB fits, and a lot of left over functionality.
* The project names are selfexplanatory except `crab`, `crab` was derived with a full, good old bumpy GSC and insitu.
* On line 793 in `modules/calibration.py` there is a really cool function called `fit_performance_plot()` that makes even cooler 2D histograms of fit variables with the respective fit. However, this function exits in a weird way and the calibration doesn't finish, so this is commented out right now.